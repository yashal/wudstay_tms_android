package com.wudstay.tms.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomRegularEditText extends EditText {
	public CustomRegularEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
		this.setTypeface(face);
	}
}
