package com.wudstay.tms.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomRegularTextView extends TextView {
    public CustomRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
        this.setTypeface(face);
    }
}
