package com.wudstay.tms.activity;

import android.os.Bundle;

import com.wudstay.tms.R;
import com.wudstay.tms.utils.WudStayConstants;

public class DashboardActivity extends BaseActivity {

    private DashboardActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        WudStayConstants.ACTIVITIES.add(ctx);

    }
}
