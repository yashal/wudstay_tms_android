package com.wudstay.tms.activity;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.GridView;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.PropertySpinnerAdapter;
import com.wudstay.tms.adapter.UploadImageAdapter;
import com.wudstay.tms.adapter.ViewDocsAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.TenantPropertyListPojo;
import com.wudstay.tms.pojo.ViewDocPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yash on 4/19/2017.
 */

public class ViewDocs extends BaseActivity {

    private ViewDocs ctx = this;
    private GridView image_grid;
    private ViewDocsAdapter adapter;
    private WudstayDialogs dialog;
    private ConnectionDetector cd;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_docs);

        WudStayConstants.ACTIVITIES.add(ctx);
        setDrawerAndToolbar("View eKYC Documents");

        dialog = new WudstayDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());

        id = ""+getIntent().getIntExtra("id", 0);

        getListOfImage();

    }

    private void getListOfImage()
    {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ViewDocPojo> call = apiService.viewDocs(getFromPrefs(WudStayConstants.COOKIE_NAME), "63");
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<ViewDocPojo>() {
                @Override
                public void onResponse(Call<ViewDocPojo> call, Response<ViewDocPojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().getTenantDocs() != null && response.body().getResponseObjects().getTenantDocs().size() > 0){
                            image_grid = (GridView)findViewById(R.id.image_grid);
                            adapter = new ViewDocsAdapter(ctx, response.body().getResponseObjects().getTenantDocs());
                            image_grid.setAdapter(adapter);
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<ViewDocPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        }
        else
        {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }
}
