package com.wudstay.tms.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.wudstay.tms.R;
import com.wudstay.tms.adapter.PropertyListAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.BasePojo;
import com.wudstay.tms.pojo.DashboardPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {

    private ViewPager viewPager;
    private HomeActivity ctx = this;
    private boolean doubleBackToExitPressedOnce = false;
    private ConnectionDetector cd;
    private WudstayDialogs dialog, progressDialog;
    private String email_text, password_text;
    private TextView previous_month_revenue, current_month_revenue, next_month_revenue, previous_month, current_month, next_month;
    private TextView property_count, tenants_count, complaint_counts;
    private LinearLayout PropertyLL, TenantLL, ComplainLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        WudStayConstants.ACTIVITIES.add(ctx);

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setVisibility(View.GONE);

        previous_month_revenue = (TextView) findViewById(R.id.previous_month_revenue);
        current_month_revenue = (TextView) findViewById(R.id.current_month_revenue);
        next_month_revenue = (TextView) findViewById(R.id.next_month_revenue);

        previous_month = (TextView) findViewById(R.id.previous_month);
        current_month = (TextView) findViewById(R.id.current_month);
        next_month = (TextView) findViewById(R.id.next_month);

        property_count = (TextView) findViewById(R.id.property_count);
        tenants_count = (TextView) findViewById(R.id.tenants_count);
        complaint_counts = (TextView) findViewById(R.id.complaint_counts);

        PropertyLL = (LinearLayout) findViewById(R.id.PropertyLL);
        TenantLL = (LinearLayout) findViewById(R.id.TenantLL);
        ComplainLL = (LinearLayout) findViewById(R.id.ComplainLL);

        setDrawerAndToolbar("Home");
        viewPager = (ViewPager) findViewById(R.id.propertylist_pager);

        email_text = getIntent().getStringExtra("email_text");
        password_text = getIntent().getStringExtra("password_text");

        PropertyListAdapter adapter = new PropertyListAdapter(this, /*pgDetail.getRoomImagesOthers(),*/ 600);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

        PropertyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(ctx, ViewPropertyActivity.class);
                startActivity(mIntent);
            }
        });

        TenantLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(ctx, ViewTenantActivity.class);
                startActivity(mIntent);
            }
        });

        ComplainLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(ctx, ComplaintsActivity.class);
                startActivity(mIntent);
            }
        });

        getDashboard();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void getDashboard() {
        if (cd.isConnectingToInternet()) {
            callSeekPosition();
            progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<DashboardPojo> call = apiService.getDashBoard(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<DashboardPojo>() {
                @Override
                public void onResponse(Call<DashboardPojo> call, Response<DashboardPojo> response) {
                    System.out.println("hh success retrofit URL ");

                    if (response.body().getResponseObjects().getLoggedInUserDto().getDisplayName() != null && !response.body().getResponseObjects().getLoggedInUserDto().getDisplayName().equals(""))
                        saveIntoPrefs(WudStayConstants.USER_NAME, response.body().getResponseObjects().getLoggedInUserDto().getDisplayName());

                    if (response.body().getResponseObjects().getpMonth() != null && !response.body().getResponseObjects().getpMonth().isEmpty() && response.body().getResponseObjects().getpMonthAmt() != null) {
                        previous_month.setText(response.body().getResponseObjects().getpMonth());
                        previous_month_revenue.setText(": " + getResources().getString(R.string.Rs) + " " + response.body().getResponseObjects().getpMonthAmt());
                    }
                    if (response.body().getResponseObjects().getcMonth() != null && !response.body().getResponseObjects().getcMonth().isEmpty() && response.body().getResponseObjects().getcMonthAmt() != null) {
                        current_month.setText(response.body().getResponseObjects().getcMonth());
                        current_month_revenue.setText(": " + getResources().getString(R.string.Rs) + " " + response.body().getResponseObjects().getcMonthAmt());
                    }
                    if (response.body().getResponseObjects().getnMonth() != null && !response.body().getResponseObjects().getnMonth().isEmpty() && response.body().getResponseObjects().getnMonthAmt() != null) {
                        next_month.setText(response.body().getResponseObjects().getnMonth());
                        next_month_revenue.setText(": " + getResources().getString(R.string.Rs) + " " + response.body().getResponseObjects().getnMonthAmt());
                    }

                    if (response.body().getResponseObjects().getCountProperties() != null) {
                        property_count.setText("" + response.body().getResponseObjects().getCountProperties());
                    }
                    if (response.body().getResponseObjects().getCountComplaints() != null) {
                        complaint_counts.setText("" + response.body().getResponseObjects().getCountComplaints());
                    }
                    if (response.body().getResponseObjects().getCountBookings() != null) {
                        tenants_count.setText("" + response.body().getResponseObjects().getCountBookings());
                    }

                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<DashboardPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }

    }
}
