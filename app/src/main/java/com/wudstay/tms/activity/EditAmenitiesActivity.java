package com.wudstay.tms.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import com.wudstay.tms.R;
import com.wudstay.tms.adapter.EditAmenitiesAdapter;
import com.wudstay.tms.utils.WudStayConstants;

public class EditAmenitiesActivity extends BaseActivity {

    private EditAmenitiesActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_amenities);

        WudStayConstants.ACTIVITIES.add(ctx);

        recyclerView = (RecyclerView) findViewById(R.id.amenities_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter = new EditAmenitiesAdapter(ctx);
        recyclerView.setAdapter(mAdapter);

        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
