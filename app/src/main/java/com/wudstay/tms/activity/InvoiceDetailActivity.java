package com.wudstay.tms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.wudstay.tms.R;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.CreateTPojo;
import com.wudstay.tms.pojo.OtherBasePojo;
import com.wudstay.tms.pojo.ParticularList;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import org.json.JSONException;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceDetailActivity extends BaseActivity {

    private InvoiceDetailActivity ctx = this;
    private EditText ten_name, ten_email, ten_mobile, room_rent, food, wifi;
    private LinearLayout update;
    private String tenant_name, tenant_email, tenant_mobile,  particularId, particularAmount;
    private int id, room_rent_amount, food_amount, wifi_amount, room_particular_id, food_particular_id, wifi_particular_id;
    private ArrayList<String> idArr, amountArr;
    private WudstayDialogs dialog;
    private ConnectionDetector cd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_detail);

        WudStayConstants.ACTIVITIES.add(ctx);

        LinearLayout drawerButton = (LinearLayout) findViewById(R.id.drawerButton);
        drawerButton.setVisibility(View.GONE);

        setDrawerAndToolbar("Send Invoice");

        dialog = new WudstayDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        ten_name = (EditText)findViewById(R.id.ten_name);
        ten_email = (EditText)findViewById(R.id.ten_email);
        ten_mobile = (EditText)findViewById(R.id.ten_mobile);
        room_rent = (EditText)findViewById(R.id.room_rent);
        food = (EditText)findViewById(R.id.food);
        wifi = (EditText)findViewById(R.id.wifi);
        update = (LinearLayout)findViewById(R.id.update);

        if (getIntent().getStringExtra("tenant_name") != null) {
            tenant_name = getIntent().getStringExtra("tenant_name");
            ten_name.setText(tenant_name);
        }

        if (getIntent().getStringExtra("tenant_email") != null) {
            tenant_email = getIntent().getStringExtra("tenant_email");
            ten_email.setText(tenant_email);
        }

        if (getIntent().getStringExtra("tenant_mobile") != null) {
            tenant_mobile = getIntent().getStringExtra("tenant_mobile");
            ten_mobile.setText(tenant_mobile);
        }

        room_rent_amount = getIntent().getIntExtra("room_rent_amount", 0);
        room_rent.setText(""+room_rent_amount);
        room_particular_id = getIntent().getIntExtra("room_particular_id", 0);
        food_amount = getIntent().getIntExtra("food_amount", 0);
        food.setText(""+food_amount);
        food_particular_id = getIntent().getIntExtra("food_particular_id", 0);
        wifi_amount = getIntent().getIntExtra("wifi_amount", 0);
        wifi.setText(""+wifi_amount);
        wifi_particular_id = getIntent().getIntExtra("wifi_particular_id", 0);
        id = getIntent().getIntExtra("id", 0);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("hh wifi_particular_id is "+wifi_particular_id);
                System.out.println("hh food_particular_id is "+food_particular_id);
                System.out.println("hh room_particular_id is "+room_particular_id);

                System.out.println("hh wifi_amount is "+wifi_amount);
                System.out.println("hh food_amount is "+food_amount);
                System.out.println("hh room_rent_amount is "+room_rent_amount);

                idArr = new ArrayList<>();
                amountArr = new ArrayList<>();
                idArr.add(""+room_particular_id);
                idArr.add(""+wifi_particular_id);
                idArr.add(""+food_particular_id);

                amountArr.add(room_rent.getText().toString().trim());
                amountArr.add(wifi.getText().toString().trim());
                amountArr.add(food.getText().toString().trim());
                try {
                    createJSONOBjectFromValues();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void createJSONOBjectFromValues() throws JSONException
    {
        JsonArray jsonArray = new JsonArray();

        for (int i = 0; i < idArr.size(); i++)
        {
            if (idArr.get(i) != null)
            {
                particularId = ""+idArr.get(i);
            }
            if (amountArr.get(i) != null)
            {
                particularAmount = amountArr.get(i);
            }
            try {
                JsonObject nums = new JsonObject();
                nums.addProperty("particularId", particularId);
                nums.addProperty("amount", particularAmount);
                jsonArray.add(nums);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        System.out.println("hh jsonArray size is " + jsonArray.size());
        JsonObject invoiceParticularForTenantDtos = new JsonObject();
        invoiceParticularForTenantDtos.add("invoiceParticularForTenantDtos", jsonArray);
        invoiceParticularForTenantDtos.addProperty("id", id);
        System.out.println("hh myjson is " + invoiceParticularForTenantDtos.toString());

        if (cd.isConnectingToInternet()) {
            callSeekPosition();
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<OtherBasePojo> call = apiService.createParticulars(getFromPrefs(WudStayConstants.COOKIE_NAME), getFromPrefs(WudStayConstants.TOKEN), invoiceParticularForTenantDtos);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<OtherBasePojo>() {
                @Override
                public void onResponse(Call<OtherBasePojo> call, Response<OtherBasePojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getErrorMessage() != null && response.body().getErrorMessage().equals("SUCCESS")) {
                        room_rent_amount = Integer.parseInt(room_rent.getText().toString().trim());
                        food_amount = Integer.parseInt(food.getText().toString().trim());
                        wifi_amount = Integer.parseInt(wifi.getText().toString().trim());

                        System.out.println("hh updated wifi_particular_id is "+wifi_particular_id);
                        System.out.println("hh updated food_particular_id is "+food_particular_id);
                        System.out.println("hh updated room_particular_id is "+room_particular_id);

                        System.out.println("hh updated wifi_amount is "+wifi_amount);
                        System.out.println("hh updated food_amount is "+food_amount);
                        System.out.println("hh updated room_rent_amount is "+room_rent_amount);
                    }
                    else
                    {
                        Toast.makeText(ctx, response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                    }

                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<OtherBasePojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }
}
