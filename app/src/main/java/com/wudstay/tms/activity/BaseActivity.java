package com.wudstay.tms.activity;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wudstay.tms.R;
import com.wudstay.tms.fragments.DrawerFragment;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.BasePojo;
import com.wudstay.tms.pojo.DashboardPojo;
import com.wudstay.tms.pojo.GetTokenPojo;
import com.wudstay.tms.pojo.LoginPojo;
import com.wudstay.tms.service.CheckTimerService;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by arpit on 1/5/2017.
 */

public class BaseActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private DrawerLayout mDrawerLayout;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private WudstayDialogs progressDialog;
    private BaseActivity ctx = this;
    private Handler splashTimeHandler;
    private Runnable finalizer;
    private String activityName;
    public static CountDownTimer timer = null;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            switch (intent.getAction()) {
                case CheckTimerService.ACTION_GET_TOKEN:
                    getToken();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(CheckTimerService.ACTION_GET_TOKEN);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public Toolbar setDrawerAndToolbar(String name) {
        Toolbar appbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(appbar);
        getSupportActionBar().setTitle("");
        final LinearLayout drawer_button = (LinearLayout) findViewById(R.id.drawerButton);

        TextView header = (TextView) findViewById(R.id.header);
        header.setText(name);

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                if (mDrawerLayout != null && !mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                else if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
            }
        });
        if (mDrawerLayout != null) {
            DrawerFragment fragment = (DrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_drawer);
            fragment.setUp(mDrawerLayout);
        }
        return appbar;
    }

    public boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void setImageInLayout(Context ctx, int width, int height, String url, ImageView image) {
        Picasso.with(ctx).load(url).resize(width, height).placeholder(R.mipmap.logo).error(R.mipmap.logo).into(image);
    }

    public void setProfileImageInLayout(Context ctx, int width, int height, String url, ImageView image) {
        Picasso.with(ctx).load(url).placeholder(R.mipmap.logo).error(R.mipmap.logo).into(image);
    }

    public ComponentName getComponentName() {
        ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        return cn;
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void saveIntoPrefs(String key, String value) {
        SharedPreferences prefs = getSharedPreferences(WudStayConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public void saveIntIntoPrefs(String key, int value) {
        SharedPreferences prefs = getSharedPreferences(WudStayConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(WudStayConstants.PREF_NAME, MODE_PRIVATE);
        return prefs.getString(key, WudStayConstants.DEFAULT_VALUE);
    }

    public int getIntFromPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(WudStayConstants.PREF_NAME, MODE_PRIVATE);
        return prefs.getInt(key, 0);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }
    }

    // dynamically permission added

    /**
     * method that will return whether the permission is accepted. By default it is true if the user is using a device below
     * version 23
     *
     * @param permission
     * @return
     */

    public boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canMakeSmores()) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     *
     * @param permission
     * @return
     */
    public boolean shouldWeAsk(String permission) {
        return (sharedPreferences.getBoolean(permission, true));
    }

    /**
     * we will save that we have already asked the user
     *
     * @param permission
     */
    public void markAsAsked(String permission, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, false).apply();
    }

    /**
     * We may want to ask the user again at their request.. Let's clear the
     * marked as seen preference for that permission.
     *
     * @param permission
     */
    public void clearMarkAsAsked(String permission) {
        sharedPreferences.edit().putBoolean(permission, true).apply();
    }


    /**
     * This method is used to determine the permissions we do not have accepted yet and ones that we have not already
     * bugged the user about.  This comes in handle when you are asking for multiple permissions at once.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * this will return us all the permissions we have previously asked for but
     * currently do not have permission to use. This may be because they declined us
     * or later revoked our permission. This becomes useful when you want to tell the user
     * what permissions they declined and why they cannot use a feature.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findRejectedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && !shouldWeAsk(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * Just a check to see if we have marshmallows (version 23)
     *
     * @return
     */
    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * a method that will centralize the showing of a snackbar
     */
    public void makePostRequestSnack(String message, int size) {

        Toast.makeText(getApplicationContext(), String.valueOf(size) + " " + message, Toast.LENGTH_SHORT).show();

        finish();
    }

    public void checkValidationOfUser(final String email, final String password, final String from) {
        if (cd.isConnectingToInternet()) {
            callSeekPosition();
            progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<LoginPojo> call = apiService.login(getFromPrefs(WudStayConstants.COOKIE_NAME), email, password, getFromPrefs(WudStayConstants.TOKEN));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<LoginPojo>() {
                @Override
                public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                    System.out.println("hh success retrofit URL ");

                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().isAuth()) {
                            saveIntoPrefs(WudStayConstants.EMAIL, email);
                            saveIntoPrefs(WudStayConstants.PASSWORD, password);
                            if (from.equals("Login")) {
                                Intent intent = new Intent(ctx, HomeActivity.class);
                                intent.putExtra("email_text", email);
                                intent.putExtra("password_text", password);
                                startActivity(intent);
                                finish();
                            } else {
                                goAhead(email, password);
                            }
                        } else {
                            if (response.body().getMessage() != null) {
                                Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (response.body().getMessage() != null) {
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<LoginPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }

    }

    public void goAhead(final String email, final String password) {
        splashTimeHandler = new Handler();
        finalizer = new Runnable() {
            public void run() {

                Intent mainIntent = new Intent(ctx, HomeActivity.class);
                mainIntent.putExtra("email_text", email);
                mainIntent.putExtra("password_text", password);
                startActivity(mainIntent);
                finish();
            }
        };
        splashTimeHandler.postDelayed(finalizer, 1000);
    }

    public void getToken() {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<GetTokenPojo> call = apiService.getToken(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<GetTokenPojo>() {
                @Override
                public void onResponse(Call<GetTokenPojo> call, Response<GetTokenPojo> response) {
                    System.out.println("hh success retrofit URL ");

                    // get headers
                    Headers headers = response.headers();
                    // get header value
                    if (response.headers().get("Set-Cookie") != null) {
                        String cookie = response.headers().get("Set-Cookie");
                        System.out.println("hh cookie is " + cookie);
                        String[] cookieName = cookie.split(";");
                        System.out.println("hh cookiename " + cookieName[0]);
                        saveIntoPrefs(WudStayConstants.COOKIE_NAME, cookieName[0]);
                        System.out.println("hh cookiename after saving is " + getFromPrefs(WudStayConstants.COOKIE_NAME));
                    }
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().getToken() != null) {
                            System.out.println("hh token is " + response.body().getResponseObjects().getToken());
                            saveIntoPrefs(WudStayConstants.TOKEN, response.body().getResponseObjects().getToken());
                            System.out.println("hh token after saving is " + getFromPrefs(WudStayConstants.TOKEN));
                        }
                    }
                    try {
                        ActivityInfo info = getPackageManager().getActivityInfo(ctx.getComponentName(), 0);
                        System.out.println("hh yashal activity name is " + info.name);
                        activityName = info.name;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();

                    }

                    if (activityName.equals("com.wudstay.tms.activity.SplashActivity")) {
                        System.out.println("hh value getFromPrefs(WudStayConstants.EMAIL) " + getFromPrefs(WudStayConstants.EMAIL));
                        System.out.println("hh value getFromPrefs(WudStayConstants.PASSWORD) " + getFromPrefs(WudStayConstants.PASSWORD));
                        if (getFromPrefs(WudStayConstants.EMAIL).equals("") && getFromPrefs(WudStayConstants.PASSWORD).equals("")) {
                            splashTimeHandler = new Handler();
                            finalizer = new Runnable() {
                                public void run() {

                                    Intent mainIntent = new Intent(ctx, LoginActivity.class);
                                    startActivity(mainIntent);
                                    finish();
                                }
                            };
                            splashTimeHandler.postDelayed(finalizer, 1000);
                        } else {
                            checkValidationOfUser(getFromPrefs(WudStayConstants.EMAIL), getFromPrefs(WudStayConstants.PASSWORD), "Splash");
                        }
                    } else {
                        System.out.println("hh yashal service started");
                    }

                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<GetTokenPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }

    }

    public void callSeekPosition() {
        if (BaseActivity.timer != null) {
            BaseActivity.timer.cancel();
        }
        BaseActivity.timer = new CountDownTimer(20000 * 60, 1000) {
            public void onTick(long millisUntilFinished) {
//                System.out.println("hh yashal " + millisUntilFinished);
            }

            public void onFinish() {
                if (BaseActivity.timer != null)
                    BaseActivity.timer.cancel();
                getToken();
                callSeekPosition();
            }
        }.start();
    }

    public void Logout() {
        if (cd.isConnectingToInternet()) {
            callSeekPosition();
            progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<BasePojo> call = apiService.logOut(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<BasePojo>() {
                @Override
                public void onResponse(Call<BasePojo> call, Response<BasePojo> response) {
                    System.out.println("hh success retrofit URL ");

                    if (response.body() != null && response.body().getResponseCode() == 1) {

                        if (response.body().getMessage() != null) {
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            for (int i = 0; i < WudStayConstants.ACTIVITIES.size(); i++) {
                                if (WudStayConstants.ACTIVITIES.get(i) != null)
                                    WudStayConstants.ACTIVITIES.get(i).finish();
                            }
                            System.out.println("hh Logout Successfully");
                            Intent intent = new Intent(ctx, LoginActivity.class);
                            ctx.startActivity(intent);
                            ctx.finish();
                            saveIntoPrefs(WudStayConstants.EMAIL, "");
                            saveIntoPrefs(WudStayConstants.PASSWORD, "");
                            saveIntoPrefs(WudStayConstants.COOKIE_NAME, "");
                            saveIntoPrefs(WudStayConstants.TOKEN, "");

                        }
                    } else {
                        if (response.body().getMessage() != null) {
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<BasePojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    public SimpleDateFormat getDateFormat() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        return sdf;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = ctx.getAssets().open("sample.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
