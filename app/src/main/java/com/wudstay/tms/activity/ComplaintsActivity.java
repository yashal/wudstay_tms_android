package com.wudstay.tms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.ComplaintListAdapter;
import com.wudstay.tms.adapter.ViewPropertyAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.ComplainPojo;
import com.wudstay.tms.pojo.OwnerLsitProperties;
import com.wudstay.tms.pojo.PropertyPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplaintsActivity extends BaseActivity implements View.OnClickListener {

    private ComplaintsActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private TextView no_complain_available;
    private ArrayList<OwnerLsitProperties> listOfOwnProperties;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);

        WudStayConstants.ACTIVITIES.add(ctx);
        setDrawerAndToolbar("Complaints");

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);
        listOfOwnProperties = new ArrayList<>();
        no_complain_available = (TextView) findViewById(R.id.no_complain_available);
        recyclerView = (RecyclerView) findViewById(R.id.complaint_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        viewTenantComplaints();
    }

    @Override
    public void onClick(View v) {
        OwnerLsitProperties data = (OwnerLsitProperties)v.getTag(R.string.key);
        Intent intent = new Intent(ctx, ComplaintsEditActivity.class);
        intent.putExtra("guest_name", data.getTenantOrUser().getTenantName());
        intent.putExtra("guest_email", data.getTenantOrUser().getTenantEmail());
        intent.putExtra("guest_phone", data.getTenantOrUser().getTenantMobile());
        intent.putExtra("property_name", data.getPropertyDto().getPropertyName());
        intent.putExtra("address", data.getPropertyDto().getPropertyAddress());
        intent.putExtra("create_date", data.getCreatedDate());
        intent.putExtra("last_modified_date", data.getLastModifiedDate());
        intent.putExtra("complain_type", data.getComplaintType());
        intent.putExtra("description", data.getDescription());
        intent.putExtra("current_status", data.getLatestStatus());
        startActivity(intent);
    }

    private void viewTenantComplaints()
    {
        if (cd.isConnectingToInternet()) {
            callSeekPosition();
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ComplainPojo> call = apiService.getTenantComplaints(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<ComplainPojo>() {
                @Override
                public void onResponse(Call<ComplainPojo> call, Response<ComplainPojo> response) {
                    System.out.println("hh success retrofit URL ");

                    if (response.body() != null && response.body().getResponseCode() == 1) {

                        if (response.body().getResponseObjects().getListOfOwnProperties() != null &&
                                response.body().getResponseObjects().getListOfOwnProperties().size() > 0){
                            listOfOwnProperties = response.body().getResponseObjects().getListOfOwnProperties();
                            mAdapter = new ComplaintListAdapter(ctx, listOfOwnProperties);
                            recyclerView.setAdapter(mAdapter);
                        }
                        else
                        {
                            no_complain_available.setVisibility(View.VISIBLE);
                        }

                    } else {
                        if (response.body().getMessage() != null) {
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<ComplainPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }
}
