package com.wudstay.tms.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.NotificationListAdapter;

public class IndividualActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private IndividualActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_notification);

        setDrawerAndToolbar("Send Notification");

        LinearLayout drawerButton = (LinearLayout) findViewById(R.id.drawerButton);
        drawerButton.setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.notification_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter = new NotificationListAdapter(ctx);
        recyclerView.setAdapter(mAdapter);
    }
}
