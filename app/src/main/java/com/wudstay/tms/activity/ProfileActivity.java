package com.wudstay.tms.activity;

import android.os.Bundle;

import com.wudstay.tms.R;
import com.wudstay.tms.utils.WudStayConstants;

public class ProfileActivity extends BaseActivity {

    private ProfileActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("Profile");
    }
}
