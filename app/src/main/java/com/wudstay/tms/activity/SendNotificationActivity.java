package com.wudstay.tms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.wudstay.tms.R;
import com.wudstay.tms.utils.WudStayConstants;

public class SendNotificationActivity extends BaseActivity {

    private SendNotificationActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_notification);
        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("Send Notifications");

        LinearLayout send_individual = (LinearLayout)findViewById(R.id.send_individual);
        send_individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, IndividualActivity.class);
                startActivity(intent);
            }
        });
    }
}
