package com.wudstay.tms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.MoreAmenitiesAdapter;
import com.wudstay.tms.utils.WudStayConstants;

public class MoreAmenitiesActivity extends BaseActivity {

    private MoreAmenitiesActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_amenities);

        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("");

        LinearLayout drawerButton = (LinearLayout) findViewById(R.id.drawerButton);
        drawerButton.setVisibility(View.GONE);

        TextView edit = (TextView) findViewById(R.id.edit);
        edit.setVisibility(View.VISIBLE);

        recyclerView = (RecyclerView) findViewById(R.id.amenities_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter = new MoreAmenitiesAdapter(ctx);
        recyclerView.setAdapter(mAdapter);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, EditAmenitiesActivity.class);
                startActivity(intent);
            }
        });

    }
}
