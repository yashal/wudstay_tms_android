package com.wudstay.tms.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.wudstay.tms.R;
import com.wudstay.tms.adapter.OccupancySpinnerAdapter;
import com.wudstay.tms.adapter.ParticularAdapter;
import com.wudstay.tms.adapter.PropertySpinnerAdapter;
import com.wudstay.tms.adapter.ViewTenantAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.BasePojo;
import com.wudstay.tms.pojo.CreateTPojo;
import com.wudstay.tms.pojo.ListTenantPojo;
import com.wudstay.tms.pojo.OccupancyList;
import com.wudstay.tms.pojo.ParticularList;
import com.wudstay.tms.pojo.PropertiesListPojo;
import com.wudstay.tms.pojo.TenantPropertyListPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateTenantsActivity extends BaseActivity {

    private CreateTenantsActivity ctx = this;
    private Spinner property_spinner, occupancy_spinner;
    private EditText ten_name, ten_email, ten_mobile, ten_address;
    private TextView move_in_date_text, notice_date_text;
    private LinearLayout notice_date, move_in_date, save, particularsLayout;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<ParticularList> particularsList;

    private ArrayList<PropertiesListPojo> property_list;
    private PropertySpinnerAdapter property_adapter;
    private WudstayDialogs dialog;
    private ConnectionDetector cd;
    private String security, roomRent, propertyId, occupancyId, tenantName, tenantEmail, tenantMobile, tenantAddress, particularId, particularAmount;
    private String moveInDate="";
    private String noticeDate="";
    private ArrayList<OccupancyList> occupancyList;
    private OccupancySpinnerAdapter occupancy_adapter;
    private Calendar moveInDate_myCalendar, notice_myCalendar;
    private int moveInDate_month, moveInDate_year, moveInDate_day, notice_month, notice_year, notice_day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tenants);

        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("Create Tenants");

        particularsList =  new ArrayList<>();

        property_spinner = (Spinner) findViewById(R.id.property_spinner);
        occupancy_spinner = (Spinner) findViewById(R.id.occupancy_spinner);
        ten_name = (EditText) findViewById(R.id.ten_name);
        ten_email = (EditText) findViewById(R.id.ten_email);
        ten_mobile = (EditText) findViewById(R.id.ten_mobile);
        ten_address = (EditText) findViewById(R.id.ten_address);
        move_in_date_text = (TextView) findViewById(R.id.move_in_date_text);
        notice_date_text = (TextView) findViewById(R.id.notice_date_text);
        notice_date = (LinearLayout) findViewById(R.id.notice_date);
        move_in_date = (LinearLayout) findViewById(R.id.move_in_date);
        particularsLayout = (LinearLayout) findViewById(R.id.particularsLayout);
        save = (LinearLayout) findViewById(R.id.save);
        recyclerView = (RecyclerView) findViewById(R.id.particular_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        dialog = new WudstayDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        getTenantList();

        notice_myCalendar = Calendar.getInstance();

        moveInDate_myCalendar = Calendar.getInstance();
        moveInDate_day = moveInDate_myCalendar.DAY_OF_MONTH;
        moveInDate_month = moveInDate_myCalendar.MONTH;
        moveInDate_year = moveInDate_myCalendar.YEAR;

        SimpleDateFormat sdf = getDateFormat();
        moveInDate = sdf.format(moveInDate_myCalendar.getTime());
        move_in_date_text.setText(moveInDate);

        property_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                propertyId = "" + property_list.get(position).getPropertyId();
                System.out.println("hh propertyId is " + propertyId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        occupancy_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                occupancyId = "" + occupancyList.get(position).getId();
                System.out.println("hh occupancyId is " + occupancyId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tenantName = ten_name.getText().toString();
                tenantEmail = ten_email.getText().toString();
                tenantMobile = ten_mobile.getText().toString();
                tenantAddress = ten_address.getText().toString();
                if (particularsList.get(0).getAmount() != null && !particularsList.get(0).getAmount().isEmpty())
                {
                    roomRent = particularsList.get(0).getAmount();
                }
                else
                {
                    roomRent = "";
                }
                if ( particularsList.get((particularsList.size()-1)).getAmount() != null && ! particularsList.get((particularsList.size()-1)).getAmount().isEmpty())
                {
                    security = particularsList.get((particularsList.size()-1)).getAmount();
                }
                else
                {
                    security  = "";
                }

                System.out.println("hh roomRent "+roomRent);
                System.out.println("hh security "+security);

                if (propertyId.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.property_select));
                }else if (tenantName.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.tenant_name_empty));
                }else if (tenantEmail.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.tenant_email_empty));
                }else if (!isValidEmail(tenantEmail)) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.valid_email_reg));
                }else if (tenantMobile.equals("") && !tenantMobile.matches("[0-9]+") && tenantMobile.length() != 10) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.valid_mobile_reg));
                }else if (occupancyId.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.occupancy_select));
                }else if (tenantAddress.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.tenant_address_empty));
                }else if (moveInDate.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.movein_select_empty));
                }else if (roomRent.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.room_rent_empty));
                }else if (security.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.createT_failed),
                            getResources().getString(R.string.security_rent_empty));
                }
                else
                {
                    try {
                        createJSONOBjectFromValues();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        move_in_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        notice_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noticeDatePicker();
            }
        });
    }

    private void createJSONOBjectFromValues() throws JSONException
    {
        JsonArray jsonArray = new JsonArray();

        for (int i = 0; i < particularsList.size(); i++)
        {
            if (particularsList.get(i).getAmount() != null && particularsList.get(i).getId() != null)
            {
                particularAmount = particularsList.get(i).getAmount();
                particularId = ""+particularsList.get(i).getId();
            }
            try {
                JsonObject nums = new JsonObject();
                nums.addProperty("particularId", particularId);
                nums.addProperty("amount", particularAmount);
                jsonArray.add(nums);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
//        System.out.println("hh jsonArray size is " + jsonArray.size());
        JsonObject tenantparticularDTOObj = new JsonObject();
        tenantparticularDTOObj.add("invoiceParticularForTenantDtos", jsonArray);
        tenantparticularDTOObj.addProperty("propertyId", propertyId);
        tenantparticularDTOObj.addProperty("occupancyId", occupancyId);
        tenantparticularDTOObj.addProperty("tenantName", tenantName);
        tenantparticularDTOObj.addProperty("tenantEmail", tenantEmail);
        tenantparticularDTOObj.addProperty("tenantMobile", tenantMobile);
        tenantparticularDTOObj.addProperty("tenantAddress", tenantAddress);
        tenantparticularDTOObj.addProperty("moveInDate", moveInDate);
        tenantparticularDTOObj.addProperty("noticeDate", noticeDate);
        tenantparticularDTOObj.addProperty("agreement", "This is test property only.");
//        System.out.println("hh myjson is " + tenantparticularDTOObj.toString());


        if (cd.isConnectingToInternet()) {
            callSeekPosition();
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<CreateTPojo> call = apiService.createT(getFromPrefs(WudStayConstants.COOKIE_NAME), getFromPrefs(WudStayConstants.TOKEN), tenantparticularDTOObj);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<CreateTPojo>() {
                @Override
                public void onResponse(Call<CreateTPojo> call, Response<CreateTPojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getErrorMessage() != null && response.body().getErrorMessage().equals("SUCCESS")) {
                        Intent intent = new Intent(ctx, ViewTenantActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Toast.makeText(ctx, response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                    }

                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<CreateTPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void showDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                moveInDate_myCalendar.set(year, month, day);
                moveInDate_year = year;
                moveInDate_month = month;
                moveInDate_day = day;
                updateLabel();
            }
        }, moveInDate_year, moveInDate_month, moveInDate_day);
        dialog.getDatePicker().setMinDate((System.currentTimeMillis()) - 1000);
        dialog.show();
    }

    private void updateLabel() {
        SimpleDateFormat sdf = getDateFormat();
        moveInDate = sdf.format(moveInDate_myCalendar.getTime());
        move_in_date_text.setText(moveInDate);
    }

    private void noticeDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                notice_myCalendar.set(year, month, day);
                notice_year = year;
                notice_month = month;
                notice_day = day;
                noticeUpdateLabel();
            }
        }, notice_year, notice_month, notice_day);
        dialog.getDatePicker().setMinDate((System.currentTimeMillis()) - 1000);
        dialog.show();
    }

    private void noticeUpdateLabel() {
        SimpleDateFormat sdf = getDateFormat();
        noticeDate = sdf.format(notice_myCalendar.getTime());
        notice_date_text.setText(noticeDate);
    }

    private void getTenantList() {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ListTenantPojo> call = apiService.getTenant(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<ListTenantPojo>() {
                @Override
                public void onResponse(Call<ListTenantPojo> call, Response<ListTenantPojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().getListOfProperties() != null && response.body().getResponseObjects().getListOfProperties().size() > 0) {
                            property_list = response.body().getResponseObjects().getListOfProperties();
                            property_adapter = new PropertySpinnerAdapter(ctx, response.body().getResponseObjects().getListOfProperties());
                            property_spinner.setAdapter(property_adapter);
                            property_spinner.setSelection(0);
                        }

                        if (response.body().getResponseObjects().getOccupancyList() != null && response.body().getResponseObjects().getOccupancyList().size() > 0) {
                            occupancyList = response.body().getResponseObjects().getOccupancyList();
                            occupancy_adapter = new OccupancySpinnerAdapter(ctx, response.body().getResponseObjects().getOccupancyList());
                            occupancy_spinner.setAdapter(occupancy_adapter);
                            occupancy_spinner.setSelection(0);
                        }

                        if (response.body().getResponseObjects().getParticularsList() != null && response.body().getResponseObjects().getParticularsList().size() > 0) {

                            particularsLayout.setVisibility(View.VISIBLE);
                            particularsList = response.body().getResponseObjects().getParticularsList();
                            mAdapter = new ParticularAdapter(ctx, particularsList);
                            recyclerView.setAdapter(mAdapter);
                        } else {
                            particularsLayout.setVisibility(View.GONE);
                        }

                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<ListTenantPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }
}
