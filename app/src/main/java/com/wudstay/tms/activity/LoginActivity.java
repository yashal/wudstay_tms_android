package com.wudstay.tms.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wudstay.tms.R;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.BasePojo;
import com.wudstay.tms.pojo.GetTokenPojo;
import com.wudstay.tms.pojo.LoginPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private EditText email_id, password;
    private TextView forgot_password, sign_up;
    private LinearLayout signIn_layout;
    private LoginActivity ctx = this;
    private WudstayDialogs dialog;
    private String email_text, password_text;
    private ConnectionDetector cd;
    private ProgressDialog d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inItView();
        dialog = new WudstayDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        signIn_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email_text = email_id.getText().toString();
                password_text = password.getText().toString();
                if (email_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.login_failed), getResources().getString(R.string.blank_email));
                } else if (!email_text.matches("[0-9]+") && !isValidEmail(email_text)) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.login_failed), getResources().getString(R.string.valid_email));
                } else if (password_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.login_failed), getResources().getString(R.string.blank_password));
                } else {
                    if (getFromPrefs(WudStayConstants.COOKIE_NAME).equals("") && getFromPrefs(WudStayConstants.TOKEN).equals(""))
                        getTokenLogin();
                    else
                        checkValidationOfUser(email_text, password_text, "Login");
                }
            }
        });

        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, RegistrationActivity.class);
                startActivity(intent);
            }
        });
    }

    private void inItView() {
        email_id = (EditText) findViewById(R.id.email_id);
        password = (EditText) findViewById(R.id.password);
        forgot_password = (TextView) findViewById(R.id.forgot_password);
        sign_up = (TextView) findViewById(R.id.sign_up);
        signIn_layout = (LinearLayout) findViewById(R.id.signIn_layout);
    }

    public void getTokenLogin() {
        if (cd.isConnectingToInternet()) {

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<GetTokenPojo> call = apiService.getToken(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<GetTokenPojo>() {
                @Override
                public void onResponse(Call<GetTokenPojo> call, Response<GetTokenPojo> response) {
                    System.out.println("hh success retrofit URL ");

                    // get headers
                    Headers headers = response.headers();
                    // get header value
                    if (response.headers().get("Set-Cookie") != null) {
                        String cookie = response.headers().get("Set-Cookie");
                        System.out.println("hh cookie is " + cookie);
                        String[] cookieName = cookie.split(";");
                        System.out.println("hh cookiename " + cookieName[0]);
                        saveIntoPrefs(WudStayConstants.COOKIE_NAME, cookieName[0]);
                        System.out.println("hh cookiename after saving is " + getFromPrefs(WudStayConstants.COOKIE_NAME));
                    }
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().getToken() != null) {
                            System.out.println("hh token is " + response.body().getResponseObjects().getToken());
                            saveIntoPrefs(WudStayConstants.TOKEN, response.body().getResponseObjects().getToken());
                            System.out.println("hh token after saving is " + getFromPrefs(WudStayConstants.TOKEN));
                        }
                    }

                    checkValidationOfUser(email_text, password_text, "Login");
                }

                @Override
                public void onFailure(Call<GetTokenPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());

                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }

    }


}
