package com.wudstay.tms.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.RequestListingAdapter;
import com.wudstay.tms.adapter.ViewPropertyAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.BasePojo;
import com.wudstay.tms.pojo.OccupancyWiseRoomsPojo;
import com.wudstay.tms.pojo.PropertiesListPojo;
import com.wudstay.tms.pojo.PropertyPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewPropertyActivity extends BaseActivity implements View.OnClickListener {

    private ViewPropertyActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ConnectionDetector cd;
    private WudstayDialogs dialog;
    private ArrayList<PropertiesListPojo> propertyList;
    private ArrayList<OccupancyWiseRoomsPojo> NumberOfRooms;
    private TextView no_property_available;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_property);

        WudStayConstants.ACTIVITIES.add(ctx);
        setDrawerAndToolbar("Property List");
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);
        NumberOfRooms = new ArrayList<>();

        no_property_available = (TextView) findViewById(R.id.no_property_available);
        recyclerView = (RecyclerView) findViewById(R.id.property_list_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        getPropertyList();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.root_layout:
                PropertiesListPojo data_list = (PropertiesListPojo)v.getTag(R.string.key);
                Intent intent = new Intent(ctx, PropertyListDetailsActivity.class);
                intent.putExtra("property_address", data_list.getPropertyAddress());
                intent.putExtra("pin_code", data_list.getPincode());
                intent.putExtra("property_type", data_list.getPropertyTypeName());
                intent.putExtra("city_name", data_list.getCityName());
                intent.putExtra("property_name", data_list.getPropertyName());
                intent.putExtra("no_of_rooms", data_list.getOccupancyWiseNoOfRooms());
                intent.putExtra("request_listing", data_list.getListingStatus());
                startActivity(intent);
                break;

            case R.id.request_listing:
                PropertiesListPojo data = (PropertiesListPojo)v.getTag(R.string.key);
                Intent intent_listing = new Intent(ctx, RequestListingActivity.class);
                intent_listing.putExtra("property_address", data.getPropertyAddress());
                intent_listing.putExtra("pin_code", data.getPincode());
                intent_listing.putExtra("property_type", data.getPropertyTypeName());
                intent_listing.putExtra("city_name", data.getCityName());
                intent_listing.putExtra("property_name", data.getPropertyName());
                intent_listing.putExtra("no_of_rooms", data.getOccupancyWiseNoOfRooms());
                intent_listing.putExtra("request_listing", data.getListingStatus());
                intent_listing.putExtra("property_id", data.getPropertyId());
                intent_listing.putExtra("property_image", data.getImages());
                intent_listing.putExtra("property_ref", data.getViewLinkOtherParameter());
                startActivity(intent_listing);
                break;

            default:
                break;
        }

    }

    public void getPropertyList() {
        if (cd.isConnectingToInternet()) {
            callSeekPosition();
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<PropertyPojo> call = apiService.propertyList(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<PropertyPojo>() {
                @Override
                public void onResponse(Call<PropertyPojo> call, Response<PropertyPojo> response) {
                    System.out.println("hh success retrofit URL ");

                    if (response.body() != null && response.body().getResponseCode() == 1) {

                        if (response.body().getResponseObjects().getListOfProperties() != null &&
                                response.body().getResponseObjects().getListOfProperties().size() > 0){
                            propertyList = response.body().getResponseObjects().getListOfProperties();
                            mAdapter = new ViewPropertyAdapter(ctx, propertyList);
                            recyclerView.setAdapter(mAdapter);
                        }
                        else
                        {
                            no_property_available.setVisibility(View.VISIBLE);
                        }

                    } else {
                        if (response.body().getMessage() != null) {
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<PropertyPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

}
