package com.wudstay.tms.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.utils.WudStayConstants;

public class ReceiptDetailActivity extends BaseActivity {

    private ReceiptDetailActivity ctx = this;
    private EditText ten_name, ten_email, ten_mobile, room_rent, food, wifi, receiptAmount, previousBalance, paidAmount, balance;
    private String tenant_name, tenant_email, tenant_mobile;
    private int  room_rent_amount, food_amount, wifi_amount, receipt_amount, previous_balance, paid_amount, balance_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_detail);

        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("Receipt Detail");

        ten_name = (EditText)findViewById(R.id.ten_name);
        ten_email = (EditText)findViewById(R.id.ten_email);
        ten_mobile = (EditText)findViewById(R.id.ten_mobile);
        room_rent = (EditText)findViewById(R.id.room_rent);
        food = (EditText)findViewById(R.id.food);
        wifi = (EditText)findViewById(R.id.wifi);
        receiptAmount = (EditText)findViewById(R.id.receiptAmount);
        previousBalance = (EditText)findViewById(R.id.previousBalance);
        paidAmount = (EditText)findViewById(R.id.paidAmount);
        balance = (EditText)findViewById(R.id.balance);

        if (getIntent().getStringExtra("tenant_name") != null) {
            tenant_name = getIntent().getStringExtra("tenant_name");
            ten_name.setText(tenant_name);
        }

        if (getIntent().getStringExtra("tenant_email") != null) {
            tenant_email = getIntent().getStringExtra("tenant_email");
            ten_email.setText(tenant_email);
        }

        if (getIntent().getStringExtra("tenant_mobile") != null) {
            tenant_mobile = getIntent().getStringExtra("tenant_mobile");
            ten_mobile.setText(tenant_mobile);
        }

        room_rent_amount = getIntent().getIntExtra("room_rent_amount", 0);
        room_rent.setText(""+room_rent_amount);

        food_amount = getIntent().getIntExtra("food_amount", 0);
        food.setText(""+food_amount);

        wifi_amount = getIntent().getIntExtra("wifi_amount", 0);
        wifi.setText(""+wifi_amount);

        receipt_amount = getIntent().getIntExtra("receipt_amount", 0);
        receiptAmount.setText(""+receipt_amount);

        previous_balance = getIntent().getIntExtra("previous_balance", 0);
        previousBalance.setText(""+previous_balance);

        paid_amount = getIntent().getIntExtra("paid_amount", 0);
        paidAmount.setText(""+paid_amount);

        balance_amount = getIntent().getIntExtra("balance_amount", 0);
        balance.setText(""+balance_amount);

        LinearLayout drawerButton = (LinearLayout) findViewById(R.id.drawerButton);
        drawerButton.setVisibility(View.GONE);

    }
}
