package com.wudstay.tms.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.wudstay.tms.R;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.BasePojo;
import com.wudstay.tms.pojo.GetTokenPojo;
import com.wudstay.tms.service.CheckTimerService;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import java.util.List;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Header;

public class SplashActivity extends BaseActivity {

    private Handler splashTimeHandler;
    private Runnable finalizer;
    private SplashActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getToken();
//        startService(new Intent(ctx, CheckTimerService.class));
        callSeekPosition();
    }

}
