package com.wudstay.tms.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.wudstay.tms.R;
import com.wudstay.tms.adapter.InvoiceListAdapter;
import com.wudstay.tms.adapter.NotificationListAdapter;
import com.wudstay.tms.adapter.PropertySpinnerAdapter;
import com.wudstay.tms.adapter.ViewTenantAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.CreateTPojo;
import com.wudstay.tms.pojo.InvoicePojo;
import com.wudstay.tms.pojo.OtherBasePojo;
import com.wudstay.tms.pojo.PropertiesListPojo;
import com.wudstay.tms.pojo.TenantParticularDataPojo;
import com.wudstay.tms.pojo.TenantParticularPojo;
import com.wudstay.tms.pojo.TenantPropertyListPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateInvoiceActivity extends BaseActivity implements View.OnClickListener {

    private CreateInvoiceActivity ctx = this;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    ArrayList<TenantParticularDataPojo> tenantList;
    private WudstayDialogs dialog, progressDialog;
    private ConnectionDetector cd;
    private Spinner property_spinner;
    private ArrayList<PropertiesListPojo> property_list;
    private PropertySpinnerAdapter property_adapter;
    private String propertyId;
    private TextView no_tenant_available;
    private ImageView select_all;
    private ArrayList<String> idList;
    private LinearLayout send_invoice;
    private String tenantId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_invoice);
        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("Create Invoice");

        tenantList = new ArrayList<>();
        idList = new ArrayList<>();

        dialog = new WudstayDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        property_spinner = (Spinner) findViewById(R.id.property_spinner);
        no_tenant_available = (TextView) findViewById(R.id.no_tenant_available);

        select_all = (ImageView) findViewById(R.id.select_all);

        send_invoice = (LinearLayout) findViewById(R.id.send_invoice);
        recyclerView = (RecyclerView) findViewById(R.id.invoice_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        TextView send_invoice_text = (TextView) findViewById(R.id.send_invoice_text);
        send_invoice_text.setText("Send Invoice");

        property_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                propertyId = "" + property_list.get(position).getPropertyId();
                idList.clear();
                select_all.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box));
                getTenantsWithParticulars(propertyId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < tenantList.size(); i++) {
                    if (tenantList.get(i).isStatus()) {
                        select_all.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box));
                        idList.remove("" + tenantList.get(i).getTenantId());
                        tenantList.get(i).setStatus(false);
                    } else {
                        select_all.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.checked_box));
                        idList.add("" + tenantList.get(i).getTenantId());
                        tenantList.get(i).setStatus(true);
                    }
                }
                mAdapter.notifyDataSetChanged();
                System.out.println("hh idList aaaa " + idList.toString());
            }
        });

        send_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (idList.size() > 0 )
                {
                    try {
                        createJSONOBjectFromValues();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Toast.makeText(ctx, "Please select at least one tenant", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        getInvoice();
    }

    private void getInvoice() {
        if (cd.isConnectingToInternet()) {
            progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<InvoicePojo> call = apiService.getInvoice(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<InvoicePojo>() {
                @Override
                public void onResponse(Call<InvoicePojo> call, Response<InvoicePojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().getListOfProperties() != null && response.body().getResponseObjects().getListOfProperties().size() > 0) {
                            property_list = response.body().getResponseObjects().getListOfProperties();
                            property_adapter = new PropertySpinnerAdapter(ctx, response.body().getResponseObjects().getListOfProperties());
                            property_spinner.setAdapter(property_adapter);
                            property_spinner.setSelection(0);
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<InvoicePojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.invoice_top_layout:

                TenantParticularDataPojo data = (TenantParticularDataPojo) v.getTag(R.string.data);
                Intent intent = new Intent(ctx, InvoiceDetailActivity.class);
                intent.putExtra("tenant_name", data.getTenantName());
                intent.putExtra("tenant_email", data.getTenantEmail());
                intent.putExtra("tenant_mobile", data.getTenantMobile());
                intent.putExtra("room_rent_amount", data.getRoomAmount());
                intent.putExtra("room_particular_id", data.getRoomParticularId());
                intent.putExtra("food_particular_id", data.getFoodParticularId());
                intent.putExtra("food_amount", data.getFoodAmount());
                intent.putExtra("wifi_particular_id", data.getWifiParticularId());
                intent.putExtra("wifi_amount", data.getWifiAmount());
                intent.putExtra("id", data.getTenantId());
                startActivity(intent);

                break;

            case R.id.individual_select:

                ArrayList<TenantParticularDataPojo> data_list = (ArrayList<TenantParticularDataPojo>) v.getTag(R.string.data);
                int index = (int) v.getTag(R.string.key);

                for (int i = 0; i < data_list.size(); i++) {
                    if (data_list.get(i).isStatus()) {
                        select_all.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box));
                        if (i == index) {
                            idList.remove("" + data_list.get(i).getTenantId());
                            data_list.get(i).setStatus(false);
                            break;
                        }
                    } else {
                        if (i == index) {
                            idList.add("" + data_list.get(i).getTenantId());
                            data_list.get(i).setStatus(true);
                            break;
                        }
                    }
                }
                System.out.println("hh idList " + idList.toString());
                mAdapter.notifyDataSetChanged();
                break;


            default:
                break;
        }


    }

    private void getTenantsWithParticulars(String propert_id) {
        if (cd.isConnectingToInternet()) {
            progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<TenantParticularPojo> call = apiService.getTenantsWithParticulars(getFromPrefs(WudStayConstants.COOKIE_NAME), propert_id);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<TenantParticularPojo>() {
                @Override
                public void onResponse(Call<TenantParticularPojo> call, Response<TenantParticularPojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getErrorMessage().equalsIgnoreCase("SUCCESS")) {
                        if (response.body().getTenantParticularsDtos() != null && response.body().getTenantParticularsDtos().size() > 0) {
                            tenantList = response.body().getTenantParticularsDtos();
                            no_tenant_available.setVisibility(View.GONE);
                            mAdapter = new InvoiceListAdapter(ctx, tenantList, "Invoice");
                            recyclerView.setAdapter(mAdapter);
                        } else {
                            no_tenant_available.setVisibility(View.VISIBLE);
                            clear();
                        }
                    } else {
                        no_tenant_available.setVisibility(View.VISIBLE);
                        clear();
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<TenantParticularPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    public void clear() {
        int size = this.tenantList.size();
        this.tenantList.clear();
        mAdapter.notifyItemRangeRemoved(0, size);
    }
    private void createJSONOBjectFromValues() throws JSONException
    {
        JSONArray jsArray = new JSONArray(idList);

        System.out.println("hh jsonArray size is " + jsArray.length());
        JSONObject tenantparticularDTOObj = new JSONObject();
        tenantparticularDTOObj.put("tenantIds", jsArray);
        tenantparticularDTOObj.put("propertyId", propertyId);
        System.out.println("hh JSONObject "+tenantparticularDTOObj.toString());
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject)jsonParser.parse(tenantparticularDTOObj.toString());
        System.out.println("hh JsonObject "+gsonObject.toString());

        if (cd.isConnectingToInternet()) {
            callSeekPosition();
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<OtherBasePojo> call = apiService.createInvoice(getFromPrefs(WudStayConstants.COOKIE_NAME), getFromPrefs(WudStayConstants.TOKEN), gsonObject);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<OtherBasePojo>() {
                @Override
                public void onResponse(Call<OtherBasePojo> call, Response<OtherBasePojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getErrorMessage() != null && response.body().getErrorMessage().equals("SUCCESS")) {
                        Toast.makeText(ctx, response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(ctx, response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                    }

                    progressDialog.getlDialog().dismiss();
                    idList.clear();
                    select_all.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box));
                     for (int i = 0; i < tenantList.size(); i++)
                     {
                         tenantList.get(i).setStatus(false);
                     }
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<OtherBasePojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

}
