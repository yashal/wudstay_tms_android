package com.wudstay.tms.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.wudstay.tms.R;
import com.wudstay.tms.adapter.PropertySpinnerAdapter;
import com.wudstay.tms.adapter.ViewPropertyAdapter;
import com.wudstay.tms.adapter.ViewTenantAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.PropertiesListPojo;
import com.wudstay.tms.pojo.TenantDtoListPojo;
import com.wudstay.tms.pojo.TenantListPojo;
import com.wudstay.tms.pojo.TenantPropertyListPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewTenantActivity extends BaseActivity implements View.OnClickListener {

    private ViewTenantActivity ctx = this;
    private WudstayDialogs dialog;
    private ConnectionDetector cd;
    private Spinner property_spinner;
    private ArrayList<PropertiesListPojo> property_list;
    private PropertySpinnerAdapter property_adapter;
    private String propertyId;
    private TextView no_tenant_available;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<TenantDtoListPojo> tenantList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_tenant);

        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("View Tenant");

        dialog = new WudstayDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        tenantList = new ArrayList<>();

        property_spinner = (Spinner) findViewById(R.id.property_spinner);
        no_tenant_available = (TextView) findViewById(R.id.no_tenant_available);

        recyclerView = (RecyclerView) findViewById(R.id.property_list_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        getTenantPropertyList();

        property_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                propertyId = ""+property_list.get(position).getPropertyId();
                getTenantList(propertyId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getTenantPropertyList()
    {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<TenantPropertyListPojo> call = apiService.getTenantPropertyList(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<TenantPropertyListPojo>() {
                @Override
                public void onResponse(Call<TenantPropertyListPojo> call, Response<TenantPropertyListPojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().getListOfProperties() != null && response.body().getResponseObjects().getListOfProperties().size() > 0){
                            property_list = response.body().getResponseObjects().getListOfProperties();
                            property_adapter = new PropertySpinnerAdapter(ctx, response.body().getResponseObjects().getListOfProperties());
                            property_spinner.setAdapter(property_adapter);
                            property_spinner.setSelection(0);
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<TenantPropertyListPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        }
        else
        {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }
    private void getTenantList(String property_id)
    {
        if (cd.isConnectingToInternet()) {
            final WudstayDialogs progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<TenantListPojo> call = apiService.getTenantList(getFromPrefs(WudStayConstants.COOKIE_NAME), property_id);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<TenantListPojo>() {
                @Override
                public void onResponse(Call<TenantListPojo> call, Response<TenantListPojo> response) {
                    System.out.println("hh success retrofit URL ");

                    if (response.body() != null && response.body().getErrorMessage() != null && response.body().getErrorMessage().equals("SUCCESS")) {
                        if (response.body().getTenantDtoList()!= null &&
                                response.body().getTenantDtoList().size() > 0){
                            tenantList = response.body().getTenantDtoList();
                            no_tenant_available.setVisibility(View.GONE);
                            mAdapter = new ViewTenantAdapter(ctx, tenantList);
                            recyclerView.setAdapter(mAdapter);
                        }
                        else
                        {
                            no_tenant_available.setVisibility(View.VISIBLE);
                            clear();
                        }
                    }
                    else
                    {
                        no_tenant_available.setVisibility(View.VISIBLE);
                        clear();
                    }

                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<TenantListPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        }
        else
        {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    public void clear() {
        int size = this.tenantList.size();
        this.tenantList.clear();
        mAdapter.notifyItemRangeRemoved(0, size);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_docs:
                int id = (int) v.getTag(R.string.key);
                Intent i = new Intent(ctx, ViewDocs.class);
                i.putExtra("id", id);
                startActivity(i);
                break;

            default:
                break;
        }
    }
}
