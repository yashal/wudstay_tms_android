package com.wudstay.tms.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.LinkAddress;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.CityListAdapter;
import com.wudstay.tms.adapter.FlatAdapter;
import com.wudstay.tms.adapter.OccupancyAdapter;
import com.wudstay.tms.adapter.PgAdapter;
import com.wudstay.tms.adapter.UserListAdapter;
import com.wudstay.tms.adapter.ViewPropertyAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.CityListPojo;
import com.wudstay.tms.pojo.GetPropertyPojo;
import com.wudstay.tms.pojo.OccupancyWiseNoOfRooms;
import com.wudstay.tms.pojo.PropertyPojo;
import com.wudstay.tms.pojo.UserTypeList;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatePropertyActivity extends BaseActivity{

    private CreatePropertyActivity ctx = this;
    private Spinner property_category_spinner, property_type_spinner, city_name_spinner;
    private ConnectionDetector cd;
    private WudstayDialogs dialog, progressDialog;
    private ArrayList<CityListPojo> ListOfCities;
    private CityListAdapter city_adapter;
    private ArrayList<OccupancyWiseNoOfRooms> flatType, pgType;

    private RecyclerView recyclerViewPg, flat_recycler_view;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private LinearLayout room_details, flat_details;
    public LinearLayout create_property;
    private String value="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_property);

        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("Create Property");

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new WudstayDialogs(ctx);

        flatType = new ArrayList<>();
        pgType = new ArrayList<>();

        property_category_spinner = (Spinner) findViewById(R.id.property_category_spinner);
        property_type_spinner = (Spinner) findViewById(R.id.property_type_spinner);
        city_name_spinner = (Spinner) findViewById(R.id.city_name_spinner);

        room_details = (LinearLayout) findViewById(R.id.room_details);
        flat_details = (LinearLayout) findViewById(R.id.flat_details);

        recyclerViewPg = (RecyclerView) findViewById(R.id.pg_recycler_view);
        recyclerViewPg.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerViewPg.setLayoutManager(mLayoutManager);
        recyclerViewPg.setNestedScrollingEnabled(false);


        flat_recycler_view = (RecyclerView) findViewById(R.id.flat_recycler_view);
        flat_recycler_view.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        flat_recycler_view.setLayoutManager(mLayoutManager);
        flat_recycler_view.setNestedScrollingEnabled(false);

        getProperty();

        if (pgType.size() > 0) {
            flat_details.setVisibility(View.GONE);
            room_details.setVisibility(View.VISIBLE);
            mAdapter = new PgAdapter(ctx, pgType);
            recyclerViewPg.setAdapter(mAdapter);
        }
        ArrayAdapter<CharSequence> adapter_property_category = ArrayAdapter.createFromResource(ctx, R.array.property_category, R.layout.my_spinner_textview);
        adapter_property_category.setDropDownViewResource(R.layout.my_spinner_textview);

        property_category_spinner.setAdapter(adapter_property_category);
        property_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = parent.getItemAtPosition(position).toString();

                System.out.println("hh r2c2 " + spinnerStr);
                if (spinnerStr.equalsIgnoreCase("Pg")) {
                    if (pgType.size() > 0) {
                        flat_details.setVisibility(View.GONE);
                        room_details.setVisibility(View.VISIBLE);
                        mAdapter = new PgAdapter(ctx, pgType);
                        recyclerViewPg.setAdapter(mAdapter);
                    }
                } else if (spinnerStr.equalsIgnoreCase("Flat")) {
                    flat_details.setVisibility(View.VISIBLE);
                    room_details.setVisibility(View.GONE);
                    mAdapter = new FlatAdapter(ctx, flatType);
                    flat_recycler_view.setAdapter(mAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter_property_type = ArrayAdapter.createFromResource(ctx, R.array.property_type, R.layout.my_spinner_textview);
        adapter_property_type.setDropDownViewResource(R.layout.my_spinner_textview);

        property_type_spinner.setAdapter(adapter_property_type);
        property_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = parent.getItemAtPosition(position).toString();

                System.out.println("hh r2c2 " + spinnerStr);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        create_property = (LinearLayout) findViewById(R.id.create_property);

        create_property.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(ctx, PropertyDetailActivity.class);
//                startActivity(intent);

                for (int i = 0; i < pgType.size(); i++)
                {
                    if (pgType.get(i).getNoOfRooms() != null)
                    {
                        value += pgType.get(i).getOccupancyTypeId()+"|"+pgType.get(i).getNoOfBeds()+"|"+pgType.get(i).getOccupancyName()
                                +"|"+pgType.get(i).getNoOfRooms()+"~";
                    }

                }
                System.out.println("hh arpitjain "+value);
                System.out.println("hh arpitjain "+value.substring(0, (value.length()-1)));
            }
        });


    }

    private void getProperty() {
        if (cd.isConnectingToInternet()) {
            callSeekPosition();
            progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<GetPropertyPojo> call = apiService.getProperty(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<GetPropertyPojo>() {
                @Override
                public void onResponse(Call<GetPropertyPojo> call, Response<GetPropertyPojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().getListOfCities() != null && response.body().getResponseObjects().getListOfCities().size() > 0) {
                            ListOfCities = response.body().getResponseObjects().getListOfCities();
                            UserTypeList slp = new UserTypeList();
//                            arrUserType.add(0, slp);
                            city_adapter = new CityListAdapter(ctx, ListOfCities);
                            city_name_spinner.setAdapter(city_adapter);
                            city_name_spinner.setSelection(1);
                        }

                        if (response.body().getResponseObjects().getPropertyForm().getOccupancyWiseNoOfRooms() != null && response.body().getResponseObjects().getPropertyForm().getOccupancyWiseNoOfRooms().size() > 0) {
                            for (int i = 0; i < response.body().getResponseObjects().getPropertyForm().getOccupancyWiseNoOfRooms().size(); i++) {
                                if (response.body().getResponseObjects().getPropertyForm().getOccupancyWiseNoOfRooms().get(i).getCategory().equalsIgnoreCase("Pg")) {
                                    pgType.add(response.body().getResponseObjects().getPropertyForm().getOccupancyWiseNoOfRooms().get(i));
                                } else if (response.body().getResponseObjects().getPropertyForm().getOccupancyWiseNoOfRooms().get(i).getCategory().equalsIgnoreCase("Flat")) {
                                    flatType.add(response.body().getResponseObjects().getPropertyForm().getOccupancyWiseNoOfRooms().get(i));
                                }
                            }
                            flat_details.setVisibility(View.GONE);
                            room_details.setVisibility(View.VISIBLE);
                            mAdapter = new PgAdapter(ctx, pgType);
                            recyclerViewPg.setAdapter(mAdapter);
                            System.out.println("hhhhh pgType array size is " + pgType.size() + " flatType size is " + flatType.size());
                        }
                    }

                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<GetPropertyPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

}
