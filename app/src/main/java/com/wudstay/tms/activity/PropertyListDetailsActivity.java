package com.wudstay.tms.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.OccupancyAdapter;
import com.wudstay.tms.adapter.ViewPropertyAdapter;
import com.wudstay.tms.pojo.OccupancyWiseRoomsPojo;
import com.wudstay.tms.utils.WudStayConstants;

import java.util.ArrayList;

public class PropertyListDetailsActivity extends BaseActivity {

    private Activity ctx = this;
    private String str_property_address, str_pin_code, str_property_type, str_city_name, str_property_name;
    private ArrayList<OccupancyWiseRoomsPojo> NumberOfRooms;
    private TextView property_address, pin_code, property_type, city_name;
    private LinearLayout request_listing, occupancy_title;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private int listingStatus;
    private View view1, view2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.property_list_details);

        WudStayConstants.ACTIVITIES.add(ctx);
        NumberOfRooms = new ArrayList<>();
        NumberOfRooms.clear();

        System.out.println("xx propertyName "+getIntent().getStringExtra("property_name"));
        if (getIntent().getStringExtra("property_name") != null)
        {
            str_property_name = getIntent().getStringExtra("property_name");
            setDrawerAndToolbar(str_property_name);
        }
        else
        {
            setDrawerAndToolbar("");
        }
        if ((ArrayList<OccupancyWiseRoomsPojo>)getIntent().getSerializableExtra("no_of_rooms")!= null)
        {
            NumberOfRooms = (ArrayList<OccupancyWiseRoomsPojo>)getIntent().getSerializableExtra("no_of_rooms");
        }
        property_address = (TextView)findViewById(R.id.property_address);
        pin_code = (TextView)findViewById(R.id.pin_code);
        property_type = (TextView)findViewById(R.id.property_type);
        city_name = (TextView)findViewById(R.id.city_name);
        request_listing = (LinearLayout) findViewById(R.id.request_listing);
        occupancy_title = (LinearLayout) findViewById(R.id.occupancy_title);
        view1 = (View) findViewById(R.id.view1);
        view2 = (View) findViewById(R.id.view2);

        if (getIntent().getStringExtra("property_address") != null)
        {
            str_property_address = getIntent().getStringExtra("property_address");
            property_address.setText(str_property_address);
        }
        if (getIntent().getStringExtra("pin_code") != null)
        {
            str_pin_code = getIntent().getStringExtra("pin_code");
            pin_code.setText(str_pin_code);
        }
        if (getIntent().getStringExtra("property_type") != null)
        {
            str_property_type = getIntent().getStringExtra("property_type");
            property_type.setText(str_property_type);
        }
        if (getIntent().getStringExtra("city_name") != null)
        {
            str_city_name = getIntent().getStringExtra("city_name");
            city_name.setText(str_city_name);
        }
        listingStatus = getIntent().getIntExtra("request_listing", 0);
        if (listingStatus == 0)
        {
            request_listing.setVisibility(View.VISIBLE);
        }
        else
        {
            request_listing.setVisibility(View.GONE);
        }
        System.out.println("xxxx NumberOfRooms "+NumberOfRooms.size());
        recyclerView = (RecyclerView) findViewById(R.id.occupancy_list_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        if (NumberOfRooms.size() >0 ){
            occupancy_title.setVisibility(View.VISIBLE);
            view1.setVisibility(View.VISIBLE);
            view2.setVisibility(View.VISIBLE);
            mAdapter = new OccupancyAdapter(ctx, NumberOfRooms, "PropertyListDetailsActivity");
            recyclerView.setAdapter(mAdapter);
        }
        else
        {
            occupancy_title.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
        }

        LinearLayout drawerButton = (LinearLayout) findViewById(R.id.drawerButton);
        drawerButton.setVisibility(View.GONE);
        TextView edit = (TextView) findViewById(R.id.edit);
        edit.setVisibility(View.VISIBLE);

    }
}
