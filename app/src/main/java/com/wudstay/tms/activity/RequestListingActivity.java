package com.wudstay.tms.activity;

import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.RequestListingAdapter;
import com.wudstay.tms.pojo.OccupancyWiseRoomsPojo;
import com.wudstay.tms.utils.DepthPageTransformer;
import com.wudstay.tms.utils.WudStayConstants;

import java.util.ArrayList;

public class RequestListingActivity extends BaseActivity implements View.OnClickListener{
    private RequestListingActivity ctx = this;
    private ViewPager viewPager;
    private RequestListingAdapter pagerAdapter;
    private TextView detail_text, image_upload_text, other_detail_text;
    private LinearLayout detail_Layout, image_upload_layout,other_detail_layout,
                         other_detail_selector, detail_selector, image_upload_selector;
    public String str_property_address, str_pin_code, str_property_type, str_city_name, str_property_name,property_ref;
    public ArrayList<OccupancyWiseRoomsPojo> NumberOfRooms;
    public int listingStatus;
    public ArrayList<String> imageArr;
    public int property_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_listing);
        WudStayConstants.ACTIVITIES.add(ctx);
        if (getIntent().getStringExtra("property_name") != null)
        {
            str_property_name = getIntent().getStringExtra("property_name");
            setDrawerAndToolbar(str_property_name);
        }
        else
        {
            setDrawerAndToolbar("");
        }
        if ((ArrayList<OccupancyWiseRoomsPojo>)getIntent().getSerializableExtra("no_of_rooms")!= null)
        {
            NumberOfRooms = (ArrayList<OccupancyWiseRoomsPojo>)getIntent().getSerializableExtra("no_of_rooms");
        }
        else
        {
            NumberOfRooms = new ArrayList<>();
        }
        if ((ArrayList<String>)getIntent().getSerializableExtra("property_image")!= null)
        {
            imageArr = (ArrayList<String>)getIntent().getSerializableExtra("property_image");
        }
        else
        {
            imageArr = new ArrayList<>();
        }

        if (getIntent().getStringExtra("property_ref") != null)
        {
            property_ref = getIntent().getStringExtra("property_ref");
        }
        if (getIntent().getStringExtra("property_address") != null)
        {
            str_property_address = getIntent().getStringExtra("property_address");
        }
        if (getIntent().getStringExtra("pin_code") != null)
        {
            str_pin_code = getIntent().getStringExtra("pin_code");
        }
        if (getIntent().getStringExtra("property_type") != null)
        {
            str_property_type = getIntent().getStringExtra("property_type");
        }
        if (getIntent().getStringExtra("city_name") != null)
        {
            str_city_name = getIntent().getStringExtra("city_name");
        }
        listingStatus = getIntent().getIntExtra("request_listing", 0);

        property_id = getIntent().getIntExtra("property_id", 0);


        detail_text = (TextView)findViewById(R.id.tab1_text);
        image_upload_text = (TextView)findViewById(R.id.tab2_text);
        other_detail_text = (TextView)findViewById(R.id.tab3_text);

        detail_Layout = (LinearLayout) findViewById(R.id.tab1_layout);
        image_upload_layout = (LinearLayout) findViewById(R.id.tab2_layout);
        other_detail_layout = (LinearLayout) findViewById(R.id.tab3_layout);

        detail_selector = (LinearLayout) findViewById(R.id.tab1_selector);
        image_upload_selector = (LinearLayout) findViewById(R.id.tab2_selector);
        other_detail_selector = (LinearLayout) findViewById(R.id.tab3_selector);

        viewPager = (ViewPager) findViewById(R.id.request_listing_viewpager);

        detail_text.setText(getResources().getString(R.string.basic_details));
        image_upload_text.setText(getResources().getString(R.string.upload_images));
        other_detail_text.setText(getResources().getString(R.string.property_details));

        detail_Layout.setOnClickListener(this);
        image_upload_layout.setOnClickListener(this);
        other_detail_layout.setOnClickListener(this);

        pagerAdapter = new RequestListingAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setPageTransformer(true, new DepthPageTransformer());

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                setStyle(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    private void setStyle(int position) {
        if (position == 0) {
            detail_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.selected_tab_text_color));
            image_upload_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            other_detail_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));

            detail_selector.setVisibility(View.VISIBLE);
            image_upload_selector.setVisibility(View.GONE);
            other_detail_selector.setVisibility(View.GONE);
        } else if (position == 1) {
            detail_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            image_upload_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.selected_tab_text_color));
            other_detail_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));

            detail_selector.setVisibility(View.GONE);
            image_upload_selector.setVisibility(View.VISIBLE);
            other_detail_selector.setVisibility(View.GONE);
        }else if (position == 2) {
            detail_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            image_upload_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            other_detail_text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.selected_tab_text_color));

            detail_selector.setVisibility(View.GONE);
            image_upload_selector.setVisibility(View.GONE);
            other_detail_selector.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab1_layout:
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                    setStyle(0);
                }
                break;

            case R.id.tab2_layout:
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                    setStyle(1);
                }
                break;

            case R.id.tab3_layout:
                if (viewPager.getCurrentItem() != 2) {
                    viewPager.setCurrentItem(2);
                    setStyle(2);
                }
                break;

            default:
                break;
        }
    }
}
