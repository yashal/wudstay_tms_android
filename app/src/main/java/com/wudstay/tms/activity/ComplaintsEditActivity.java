package com.wudstay.tms.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.SpinnerListAdapter;
import com.wudstay.tms.utils.WudStayConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ComplaintsEditActivity extends BaseActivity  {

    private ComplaintsEditActivity ctx = this;
    private Spinner status_spinner;
    private ArrayList<HashMap<String, String>> statusList;
    private SpinnerListAdapter state_adapter;
    private String value;
    private String guest_name, guest_email, guest_phone, property_name, address, create_date, complain_type,
            last_modified_date, description, current_status;
    private TextView guestName, guestEmail, guestPhone, propertyName, addressTxt, createdDate, lastModifiedDate, complaintType, descriptionTxt,
                currentStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_edit);

        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("Edit Complaint");

        guestName = (TextView)findViewById(R.id.guestName);
        guestEmail = (TextView)findViewById(R.id.guestEmail);
        guestPhone = (TextView)findViewById(R.id.guestPhone);
        propertyName = (TextView)findViewById(R.id.propertyName);
        addressTxt = (TextView)findViewById(R.id.address);
        createdDate = (TextView)findViewById(R.id.createdDate);
        lastModifiedDate = (TextView)findViewById(R.id.lastModifiedDate);
        complaintType = (TextView)findViewById(R.id.complaintType);
        descriptionTxt = (TextView)findViewById(R.id.description);
        currentStatus = (TextView)findViewById(R.id.currentStatus);

        status_spinner = (Spinner)findViewById(R.id.newstatus_spinner);
        loadNewStatus();

        if (getIntent().getStringExtra("guest_name") != null)
        {
            guest_name = getIntent().getStringExtra("guest_name");
            guestName.setText(guest_name);
        }
        if (getIntent().getStringExtra("guest_email") != null)
        {
            guest_email = getIntent().getStringExtra("guest_email");
            guestEmail.setText(guest_email);
        }
        if (getIntent().getStringExtra("complain_type") != null)
        {
            complain_type = getIntent().getStringExtra("complain_type");
            complaintType.setText(complain_type);
        }
        if (getIntent().getStringExtra("guest_phone") != null)
        {
            guest_phone = getIntent().getStringExtra("guest_phone");
            guestPhone.setText(guest_phone);
        }
        if (getIntent().getStringExtra("property_name") != null)
        {
            property_name = getIntent().getStringExtra("property_name");
            propertyName.setText(property_name);
        }
        if (getIntent().getStringExtra("address") != null)
        {
            address = getIntent().getStringExtra("address");
            addressTxt.setText(address);
        }
        if (getIntent().getStringExtra("create_date") != null)
        {
            create_date = getIntent().getStringExtra("create_date");
            createdDate.setText(create_date);
        }
        if (getIntent().getStringExtra("last_modified_date") != null)
        {
            last_modified_date = getIntent().getStringExtra("last_modified_date");
            lastModifiedDate.setText(last_modified_date);
        }
        if (getIntent().getStringExtra("description") != null)
        {
            description = getIntent().getStringExtra("description");
            descriptionTxt.setText(description);
        }
        if (getIntent().getStringExtra("current_status") != null)
        {
            current_status = getIntent().getStringExtra("current_status");
            currentStatus.setText(current_status);
        }

        LinearLayout drawerButton = (LinearLayout) findViewById(R.id.drawerButton);
        drawerButton.setVisibility(View.GONE);

        status_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = statusList.get(position).get("name");
                value = statusList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx superBuildAreaUnit " + spinnerStr+" "+value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void loadNewStatus()
    {

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("complain_status");
            statusList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Log.d("Details-->", jo_inside.getString("name"));
                Log.d("Details-->", jo_inside.getString("value"));
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                statusList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(ctx, statusList);
            status_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
