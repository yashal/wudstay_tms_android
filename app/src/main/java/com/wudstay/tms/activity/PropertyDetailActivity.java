package com.wudstay.tms.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.wudstay.tms.R;
import com.wudstay.tms.utils.WudStayConstants;

public class PropertyDetailActivity extends BaseActivity {

    private PropertyDetailActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_details);

        WudStayConstants.ACTIVITIES.add(ctx);

        setDrawerAndToolbar("Create Property");

        LinearLayout drawerButton = (LinearLayout) findViewById(R.id.drawerButton);
        drawerButton.setVisibility(View.GONE);
    }
}
