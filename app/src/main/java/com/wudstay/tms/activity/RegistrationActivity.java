package com.wudstay.tms.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wudstay.tms.R;
import com.wudstay.tms.adapter.UserListAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.BasePojo;
import com.wudstay.tms.pojo.GetTokenPojo;
import com.wudstay.tms.pojo.GetUserTyepPojo;
import com.wudstay.tms.pojo.UserTypeList;
import com.wudstay.tms.pojo.ValidateUserPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.ImageLoadingUtils;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class RegistrationActivity extends BaseActivity {

    private ImageView profile_image, back;
    private EditText owner_name, mobile, email, password, confirm_password;
    private LinearLayout save;
    private RegistrationActivity ctx  = this;
    private WudstayDialogs dialog, progressDialog;
    private ConnectionDetector cd;

    private final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 456;
    private final int SELECT_PHOTO = 457;
    private String mCurrentPhotoPath;
    private String photoPath = "";

    private static final String CAMERA_DIR = "/dcim/";

    // for external permission
    private final static int READ_EXTERNAL_STORAGE = 105;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;
    private String owner_name_text, mobile_text, email_text, password_text, confirm_password_text;
    private EditText otp_code;
    private Dialog DialogOTP;
    private final static int RECEIVE_SMS_RESULT = 102;

    private ArrayList<UserTypeList> arrUserType;
    private UserListAdapter state_adapter;
    private Spinner user_spinner;
    private String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        inItView();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        dialog = new WudstayDialogs(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getUserType();

        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        addPermissionDialogMarshMallow();
                    } else {
                        showChooserDialog();
                    }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                owner_name_text = owner_name.getText().toString();
                mobile_text = mobile.getText().toString();
                email_text = email.getText().toString();
                password_text = password.getText().toString();
                confirm_password_text = confirm_password.getText().toString();
                if (owner_name_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.blank_owner_name));
                }else if (mobile_text.equals("") && !mobile_text.matches("[0-9]+") && mobile_text.length() != 10) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.valid_mobile_reg));
                }else if (email_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.blank_email_reg));
                }else if (!isValidEmail(email_text)) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.valid_email_reg));
                }else if (password_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.blank_password));
                }else if (password_text.length() < 6) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.password_length));
                }else if (confirm_password_text.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.blank_confirm_password));
                }else if (!confirm_password_text.equals(password_text)) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.registration_failed), getResources().getString(R.string.mismatch_password));
                }
                else
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        addPermissionDialogMarshMallowForSMS();
                    } else {
                        goNext();
                    }
                }
            }
        });

        user_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                value = arrUserType.get(position).getOtherValue();
                Toast.makeText(parent.getContext(), value, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void inItView()
    {
        profile_image = (ImageView)findViewById(R.id.profile_image);
        back = (ImageView)findViewById(R.id.back);
        owner_name = (EditText) findViewById(R.id.owner_name);
        mobile = (EditText) findViewById(R.id.mobile);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirm_password = (EditText) findViewById(R.id.confirm_password);
        save = (LinearLayout) findViewById(R.id.save);
        user_spinner = (Spinner) findViewById(R.id.user_spinner);
    }

    private void addPermissionDialogMarshMallow() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(WRITE_EXTERNAL_STORAGE);
        resultCode = READ_EXTERNAL_STORAGE;


        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    showChooserDialog();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case READ_EXTERNAL_STORAGE:
                if (hasPermission(WRITE_EXTERNAL_STORAGE)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    showChooserDialog();
                } else {
                    permissionsRejected.add(WRITE_EXTERNAL_STORAGE);
                    clearMarkAsAsked(WRITE_EXTERNAL_STORAGE);
                    String message = "permission for access external storage was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
            case RECEIVE_SMS_RESULT:
                if (ctx.hasPermission(RECEIVE_SMS)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    goNext();
                } else {
                    permissionsRejected.add(RECEIVE_SMS);
                    ctx.clearMarkAsAsked(RECEIVE_SMS);
                    String message = "permission for send and view SMS was rejected. Please allow to run the app.";
                    ctx.makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }

    }

    public void showChooserDialog() {

        final Dialog dialogr = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogr.setContentView(R.layout.custom_imageupload_dialog);
        dialogr.setCancelable(true);

        // set the custom dialog components - text, image and button
        ImageView gallery = (ImageView) dialogr.findViewById(R.id.galleryimage);
        gallery.setImageResource(R.mipmap.gallery_icon);
        ImageView camera = (ImageView) dialogr.findViewById(R.id.cameraimage);
        camera.setImageResource(R.mipmap.camera_icon);

        gallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, SELECT_PHOTO);
                dialogr.dismiss();
                dialogr.hide();
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // give the image a name so we can store it in the phone's
                // default location
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "bmh" + timeStamp + "_";
                    File albumF = getAlbumDir();
                    File imageF = File.createTempFile(imageFileName, "bmh", albumF);


                    mCurrentPhotoPath = imageF.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageF));
                } catch (IOException e) {
                    e.printStackTrace();

                }

                startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                dialogr.dismiss();
                dialogr.hide();
            }
        });
        dialogr.show();
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraPicture");
            } else {
                storageDir = new File(Environment.getExternalStorageDirectory() + CAMERA_DIR + "CameraPicture");
            }

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        return null;
                    }
                }
            }

        } else {
        }

        return storageDir;
    }


    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imagePath1 = cursor.getString(columnIndex);

                File f = new File(compressImage(imagePath1, 1));
//                File f = new File(imagePath1);
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                profile_image.setImageBitmap(myBitmap);
                photoPath = f.getAbsolutePath();

            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

                File f = new File(compressImage(mCurrentPhotoPath, 0));
                System.out.println("hh mCurrentPhotoPath "+mCurrentPhotoPath);
//                File f = new File(mCurrentPhotoPath);
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                profile_image.setImageBitmap(myBitmap);
                photoPath = f.getAbsolutePath();
            }
        }
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = this.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public String compressImage(String imageUri , int flag) {
        String filePath;
        if(flag ==1)
        {
            filePath = getRealPathFromURI(imageUri);
        }

        filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath,options);

        int actualHeight = options.outHeight;
        System.out.println("hh actual height is "+actualHeight);
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        ImageLoadingUtils utils = new ImageLoadingUtils(this);
        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
//        options.inPurgeable = true;
//        options.inInputShareable = true;
        options.inTempStorage = new byte[16*1024];

        try{
            bmp = BitmapFactory.decodeFile(filePath, options);
        }
        catch(OutOfMemoryError exception){
            exception.printStackTrace();

        }
        try{
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        }
        catch(OutOfMemoryError exception){
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float)options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth()/2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public void getTokenSignup() {
        if (cd.isConnectingToInternet()) {

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<GetTokenPojo> call = apiService.getToken(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<GetTokenPojo>() {
                @Override
                public void onResponse(Call<GetTokenPojo> call, Response<GetTokenPojo> response) {
                    System.out.println("hh success retrofit URL ");

                    // get headers
                    Headers headers = response.headers();
                    // get header value
                    if (response.headers().get("Set-Cookie") != null) {
                        String cookie = response.headers().get("Set-Cookie");
                        System.out.println("hh cookie is " + cookie);
                        String[] cookieName = cookie.split(";");
                        System.out.println("hh cookiename " + cookieName[0]);
                        saveIntoPrefs(WudStayConstants.COOKIE_NAME, cookieName[0]);
                        System.out.println("hh cookiename after saving is " + getFromPrefs(WudStayConstants.COOKIE_NAME));
                    }
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().getToken() != null) {
                            System.out.println("hh token is " + response.body().getResponseObjects().getToken());
                            saveIntoPrefs(WudStayConstants.TOKEN, response.body().getResponseObjects().getToken());
                            System.out.println("hh token after saving is " + getFromPrefs(WudStayConstants.TOKEN));
                        }
                    }

                    validateNewUser();
                }

                @Override
                public void onFailure(Call<GetTokenPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());

                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }

    }

    private void validateNewUser()
    {
        if (cd.isConnectingToInternet()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ValidateUserPojo> call = apiService.validateNewUser(getFromPrefs(WudStayConstants.COOKIE_NAME),owner_name_text, mobile_text,
                                                                     email_text, password_text, confirm_password_text);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<ValidateUserPojo>() {
                @Override
                public void onResponse(Call<ValidateUserPojo> call, Response<ValidateUserPojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getMessage() != null)
                        {
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            showOTPDialog();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ValidateUserPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());

                }
            });
        }
        else
        {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    public void showOTPDialog() {
        LinearLayout OkButtonSubmit;
        DialogOTP = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogOTP.setContentView(R.layout.custom_otp_dialog);
        OkButtonSubmit = (LinearLayout) DialogOTP.findViewById(R.id.btn_yes_exit_LL);
        otp_code = (EditText) DialogOTP.findViewById(R.id.otp_code);
        LinearLayout CancelButtonLogout = (LinearLayout) DialogOTP.findViewById(R.id.btn_no_exit_LL);

        /*OkButtonSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                DialogOTP.dismiss();
            }
        });*/

        otp_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                int len = otp_code.getText().toString().length();

                if (len == 6) {

                    DialogOTP.dismiss();
                    getRegister(otp_code.getText().toString().trim());
                }

            }
        });

        DialogOTP.show();
    }

    // for otp process
    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("wudsta.ACTION_RECEIVED_OTP");
        bManager.registerReceiver(OTPBroadcastReceiver, intentFilter);
    }

    public void onStop() {
        super.onStop();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        bManager.unregisterReceiver(OTPBroadcastReceiver);
    }
    BroadcastReceiver OTPBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("wudsta.ACTION_RECEIVED_OTP")) {
                if (otp_code != null) {
                    otp_code.setText(intent.getStringExtra("otpcode"));
                    if (DialogOTP != null)
                    {
                        DialogOTP.dismiss();
                        getRegister(otp_code.getText().toString().trim());
                    }
                }
            }
        }
    };

    private void goNext()
    {
        if (getFromPrefs(WudStayConstants.COOKIE_NAME).equals("") && getFromPrefs(WudStayConstants.TOKEN).equals(""))
            getTokenSignup();
        else
            validateNewUser();
    }

    private void addPermissionDialogMarshMallowForSMS() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(RECEIVE_SMS);
        resultCode = RECEIVE_SMS_RESULT;

        //filter out the permissions we have already accepted
        permissionsToRequest = ctx.findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = ctx.findRejectedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    ctx.markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    goNext();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        ctx.markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    // get user type value from api
    private void getUserType()
    {
        if (cd.isConnectingToInternet()) {
            progressDialog = new WudstayDialogs(ctx);
            progressDialog.showProgressDialog(ctx);
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<GetUserTyepPojo> call = apiService.getUserType(getFromPrefs(WudStayConstants.COOKIE_NAME));
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<GetUserTyepPojo>() {
                @Override
                public void onResponse(Call<GetUserTyepPojo> call, Response<GetUserTyepPojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getResponseObjects().getUserTypeList() != null && response.body().getResponseObjects().getUserTypeList().size() > 0){
                            arrUserType = response.body().getResponseObjects().getUserTypeList();
                            UserTypeList slp = new UserTypeList();
//                            arrUserType.add(0, slp);
                            state_adapter = new UserListAdapter(ctx, arrUserType);
                            user_spinner.setAdapter(state_adapter);
                            user_spinner.setSelection(1);
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<GetUserTyepPojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        }
        else
        {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void getRegister(String otp)
    {
        if (cd.isConnectingToInternet()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<BasePojo> call = apiService.registerNewUser(getFromPrefs(WudStayConstants.COOKIE_NAME),owner_name_text, mobile_text,
                    email_text, password_text, confirm_password_text, otp, value);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<BasePojo>() {
                @Override
                public void onResponse(Call<BasePojo> call, Response<BasePojo> response) {
                    System.out.println("hh success retrofit URL ");
                    if (response.body() != null && response.body().getResponseCode() == 1) {
                        if (response.body().getMessage() != null)
                        {
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ctx, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<BasePojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());

                }
            });
        }
        else
        {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }
    }


}
