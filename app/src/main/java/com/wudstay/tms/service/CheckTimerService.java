package com.wudstay.tms.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.wudstay.tms.activity.BaseActivity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by JOHN on 7/20/2015.
 */
public class CheckTimerService extends Service {

    private static Timer timer = new Timer();
    private Context ctx;
    public static final String ACTION_GET_TOKEN = "action_get_token";
    public static final String ACTION_GET_RESET = "action_get_reset";

    public static final int SET_TOKEN = 1;
    public static final int RESET = 2;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    public void onCreate()
    {
        super.onCreate();
        ctx = this;
        startService();
    }

    private void startService()
    {
        timer.scheduleAtFixedRate(new mainTask(), 0, 20000);
    }

    private class mainTask extends TimerTask
    {
        public void run()
        {
            toastHandler.sendEmptyMessage(0);
        }
    }

    public void onDestroy()
    {
        super.onDestroy();
        Toast.makeText(this, "Service Stopped ...", Toast.LENGTH_SHORT).show();
    }

    Handler toastHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what){
                case SET_TOKEN:
                    set_token();
                    break;
                case RESET:
                    resetTimer();
                    break;
            }
        }
    };
    Messenger messenger = new Messenger(toastHandler);

    private void set_token()
    {
        System.out.println("hh set_token is called");
        Intent message = new Intent(ACTION_GET_TOKEN);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
    }


    public void resetTimer()
    {
        timer.cancel();
        timer = new Timer();
        timer.scheduleAtFixedRate(new mainTask(), 0, 20000);

        System.out.println("hh reset_timer is called");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

}

