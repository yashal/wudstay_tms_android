package com.wudstay.tms.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.RequestListingActivity;
import com.wudstay.tms.adapter.OccupancyAdapter;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class DetailsFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private View view;
    private TextView property_address, pin_code, property_type, city_name;
    private LinearLayout request_listing, occupancy_title;
    private View view1, view2;
    private RequestListingActivity activity;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_details, container, false);
        property_address = (TextView)view.findViewById(R.id.property_address);
        pin_code = (TextView)view.findViewById(R.id.pin_code);
        property_type = (TextView)view.findViewById(R.id.property_type);
        city_name = (TextView)view.findViewById(R.id.city_name);
        request_listing = (LinearLayout)view.findViewById(R.id.request_listing);
        occupancy_title = (LinearLayout)view.findViewById(R.id.occupancy_title);
        view1 = (View)view.findViewById(R.id.view1);
        view2 = (View)view.findViewById(R.id.view2);
        recyclerView = (RecyclerView) view.findViewById(R.id.occupancy_list_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        activity = (RequestListingActivity)getActivity();

        if (activity.str_property_address!= null)
        {
            property_address.setText(activity.str_property_address);
        }
        if (activity.str_pin_code!= null)
        {
            pin_code.setText(activity.str_pin_code);
        }
        if (activity.str_property_type!= null)
        {
            property_type.setText(activity.str_property_type);
        }
        if (activity.str_city_name!= null)
        {
            city_name.setText(activity.str_city_name);
        }

        System.out.println("xxxx NumberOfRooms "+activity.NumberOfRooms.size());

        if (activity.NumberOfRooms.size() >0 ){
            occupancy_title.setVisibility(View.VISIBLE);
            view1.setVisibility(View.VISIBLE);
            view2.setVisibility(View.VISIBLE);
            mAdapter = new OccupancyAdapter(getActivity(), activity.NumberOfRooms, "RequestListingActivity");
            recyclerView.setAdapter(mAdapter);
        }
        else
        {
            occupancy_title.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
        }
        return view;
    }

}