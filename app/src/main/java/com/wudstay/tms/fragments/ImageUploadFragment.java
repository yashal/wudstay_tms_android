package com.wudstay.tms.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.RequestListingActivity;
import com.wudstay.tms.adapter.UploadImageAdapter;
import com.wudstay.tms.utils.WudStayConstants;

import java.util.ArrayList;


/**
 * Created by Yash ji on 4/22/2016.
 */
public class ImageUploadFragment extends Fragment {
    private GridView grid;
    private View view;
    private UploadImageAdapter adapter;
    private RequestListingActivity activity;
    private ArrayList<String> listofImage;
    private String image_url;
    private int propertId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_upload_images, container, false);

        activity = (RequestListingActivity)getActivity();
        if (activity.imageArr != null)
        {
            listofImage = activity.imageArr;
        }

        propertId = activity.property_id;

        image_url = WudStayConstants.IMAGE_URL+propertId+"/";
        grid=(GridView)view.findViewById(R.id.fragment_image_grid);
        adapter = new UploadImageAdapter(getActivity(), listofImage, image_url);
        grid.setAdapter(adapter);

        return view;
    }

}
