package com.wudstay.tms.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.guna.libmultispinner.MultiSelectionSpinner;
import com.wudstay.tms.R;
import com.wudstay.tms.activity.RequestListingActivity;
import com.wudstay.tms.activity.ViewPropertyActivity;
import com.wudstay.tms.adapter.SpinnerListAdapter;
import com.wudstay.tms.adapter.ViewPropertyAdapter;
import com.wudstay.tms.model.ApiClient;
import com.wudstay.tms.model.ApiInterface;
import com.wudstay.tms.pojo.BasePojo;
import com.wudstay.tms.pojo.PropertyPojo;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yash ji on 4/22/2016.
 */
public class OtherDetailsFragment extends Fragment implements MultiSelectionSpinner.OnMultipleItemsSelectedListener {

    private View view;
    private Spinner super_build_spinner, build_up_area_spinner, carpet_area_spinner, balconies_spinner, property_age_spinner,
            furnishing_spinner, bathroom_spinner, bedrooms_spinner, total_floors_spinner, property_floors_spinner,
            security_deposit_spinner;
    private MultiSelectionSpinner amenities_Spinner;
    private  LinearLayout pic_date_layout, submit_listing;
    private Calendar myCalendar;
    private int int_month, int_year, int_day;
    private RequestListingActivity activity;
    private String availableDate = "";
    private TextView pic_date_text;
    private SpinnerListAdapter state_adapter;
    private WudstayDialogs dialog;
    private ArrayList<HashMap<String, String>> areaunitList, bedroomList, bathRoomList, balconiList, furnishingList, floorList,
                                                propertyFloorList, securityDepositModeList, agePropertyList;
    private ArrayList<String> amenitiesNamesArr, amenitiesValuesArr;

    private String superBuildUpArea, superBuildAreaUnit, buildUpArea, buildUpAreaUnit, carpetArea, carpetAreaUnit,bedRooms, bathrooms,
            balconies, furnishingType, totalFloors, propertyOnFloor, expectedMonthlyRent, securityDeposit, ageOfProperty,
            securityDepositMode, propertyRef, amenities;
    private EditText super_build_up_edit, build_up_edit, carpet_area_edit, monthly_rent, security_deposit_Edit;
    private ConnectionDetector cd;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_others_details, container, false);

        activity = (RequestListingActivity)getActivity();
        dialog = new WudstayDialogs(getActivity());
        cd = new ConnectionDetector(activity.getApplicationContext());

        propertyRef = activity.property_ref;

        super_build_spinner = (Spinner) view.findViewById(R.id.super_build_spinner);
        build_up_area_spinner = (Spinner) view.findViewById(R.id.build_up_area_spinner);
        carpet_area_spinner = (Spinner) view.findViewById(R.id.carpet_area_spinner);
        balconies_spinner = (Spinner) view.findViewById(R.id.balconies_spinner);
        bathroom_spinner = (Spinner) view.findViewById(R.id.bathroom_spinner);
        bedrooms_spinner = (Spinner) view.findViewById(R.id.bedrooms_spinner);
        furnishing_spinner = (Spinner) view.findViewById(R.id.furnishing_spinner);
        total_floors_spinner = (Spinner) view.findViewById(R.id.total_floors_spinner);
        property_floors_spinner = (Spinner) view.findViewById(R.id.property_floors_spinner);
        security_deposit_spinner = (Spinner) view.findViewById(R.id.security_deposit_spinner);
        property_age_spinner = (Spinner) view.findViewById(R.id.property_age_spinner);
        pic_date_layout = (LinearLayout) view.findViewById(R.id.pic_date_layout);
        activity = (RequestListingActivity)getActivity();
        pic_date_text = (TextView) view.findViewById(R.id.pic_date_text);
        amenities_Spinner = (MultiSelectionSpinner) view.findViewById(R.id.amenities_Spinner);
        super_build_up_edit = (EditText)view.findViewById(R.id.super_build_up_edit);
        build_up_edit = (EditText)view.findViewById(R.id.build_up_edit);
        carpet_area_edit = (EditText)view.findViewById(R.id.carpet_area_edit);
        monthly_rent = (EditText)view.findViewById(R.id.monthly_rent);
        security_deposit_Edit = (EditText)view.findViewById(R.id.security_deposit_Edit);
        submit_listing = (LinearLayout) view.findViewById(R.id.submit_listing);

        myCalendar = Calendar.getInstance();
        int_day = myCalendar.DAY_OF_MONTH;
        int_month = myCalendar.MONTH;
        int_year = myCalendar.YEAR;

        SimpleDateFormat sdf = activity.getDateFormat();
        availableDate = sdf.format(myCalendar.getTime());
        pic_date_text.setText(availableDate);

        loadAreaUnit();
        loadBedRooms();
        loadBalkonies();
        loadBathRooms();
        loadFurnishing();
        loadFloor();
        loadPropertyFloor();
        loadAmenities();
        loadSecurityDepositMode();
        loadPropertyAge();
        super_build_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = areaunitList.get(position).get("name");
                superBuildAreaUnit = areaunitList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx superBuildAreaUnit " + spinnerStr+" "+superBuildAreaUnit);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        build_up_area_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                final String spinnerStr = areaunitList.get(position).get("name");
                buildUpAreaUnit = areaunitList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx buildUpAreaUnit " + spinnerStr+" "+buildUpAreaUnit);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        carpet_area_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = areaunitList.get(position).get("name");
                carpetAreaUnit = areaunitList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx carpetAreaUnit " + spinnerStr+" "+carpetAreaUnit);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bedrooms_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = bedroomList.get(position).get("name");
                bedRooms = bedroomList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx bedRooms " + spinnerStr+" "+bedRooms);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bathroom_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = bathRoomList.get(position).get("name");
                bathrooms = bathRoomList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx bathrooms " + spinnerStr+" "+bathrooms);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        balconies_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = balconiList.get(position).get("name");
                balconies = balconiList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx balconies " + spinnerStr+" "+balconies);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        furnishing_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                final String spinnerStr = furnishingList.get(position).get("name");
                furnishingType = furnishingList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx furnishingType " + spinnerStr+" "+furnishingType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        String[] array = getResources().getStringArray(R.array.amenities);
        /*amenities_Spinner.setItems(array);
//        amenities_Spinner.setSelection(new int[]{2, 6});
        amenities_Spinner.setListener(this);*/

        total_floors_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = floorList.get(position).get("name");
                totalFloors = floorList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx totalFloors " + spinnerStr+" "+totalFloors);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        property_floors_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = propertyFloorList.get(position).get("name");
                propertyOnFloor = propertyFloorList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx propertyOnFloor " + spinnerStr+" "+propertyOnFloor);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // for Security Deposit spinner
        security_deposit_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = securityDepositModeList.get(position).get("name");
                securityDepositMode = securityDepositModeList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx securityDepositMode " + spinnerStr+" "+securityDepositMode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // for Property age spinner
        property_age_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String spinnerStr = agePropertyList.get(position).get("name");
                ageOfProperty = agePropertyList.get(position).get("value");
                System.out.println("xxxxxxxxxxxx ageOfProperty " + spinnerStr+" "+ageOfProperty);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        pic_date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        submit_listing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                superBuildUpArea = super_build_up_edit.getText().toString();
                buildUpArea = build_up_edit.getText().toString();
                carpetArea = carpet_area_edit.getText().toString();
                expectedMonthlyRent = monthly_rent.getText().toString();
                securityDeposit = security_deposit_Edit.getText().toString();

                if (superBuildUpArea.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.super_build_area_name));
                }else if (superBuildAreaUnit.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.super_build_area_unit));
                }else if (buildUpArea.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.build_area_name));
                }else if (buildUpAreaUnit.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.build_area_unit));
                }else if (carpetArea.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.carpet_area_name));
                }else if (carpetAreaUnit.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.carpet_area_unit));
                }else if (bedRooms.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.bedroom_select));
                }else if (bathrooms.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.bathroom_select));
                }else if (balconies.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.balconies_select));
                }else if (furnishingType.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.furnishing_type_select));
                }else if (totalFloors.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.total_floors_select));
                }else if (propertyOnFloor.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.property_floors_select));
                }else if (availableDate.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.avail_date_empty));
                }else if (expectedMonthlyRent.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.expected_monthly_rent_empty));
                }else if (securityDepositMode.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.sec_deposit_select));
                }else if (securityDeposit.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.sec_deposit_amt_empty));
                }else if (ageOfProperty.equals("")) {
                    dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.listing_failed),
                            getResources().getString(R.string.ageofproperty_select));
                }
                else
                {
                    postRequestForListing();
                }
            }
        });

        return view;
    }

    private void postRequestForListing()
    {

        if (cd.isConnectingToInternet()) {
            activity.callSeekPosition();
            final WudstayDialogs progressDialog = new WudstayDialogs(getActivity());
            progressDialog.showProgressDialog(getActivity());
            progressDialog.getlDialog().setCancelable(Boolean.FALSE);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<BasePojo> call = apiService.requestForListing(activity.getFromPrefs(WudStayConstants.COOKIE_NAME), activity.getFromPrefs(WudStayConstants.TOKEN),
                    superBuildUpArea, superBuildAreaUnit, buildUpArea, buildUpAreaUnit, carpetArea, carpetAreaUnit, bedRooms, bathrooms,
                    balconies, furnishingType, totalFloors, propertyOnFloor, availableDate, expectedMonthlyRent, securityDeposit, ageOfProperty, securityDepositMode, propertyRef, amenities);
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<BasePojo>() {
                @Override
                public void onResponse(Call<BasePojo> call, Response<BasePojo> response) {
                    System.out.println("hh success retrofit URL ");

                    if (response.body() != null && response.body().getResponseCode() == 1) {

                        Intent mIntent = new Intent(activity, ViewPropertyActivity.class);
                        startActivity(mIntent);
                        getActivity().finish();

                    } else {
                        if (response.body().getMessage() != null) {
                            Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    progressDialog.getlDialog().dismiss();
                }

                @Override
                public void onFailure(Call<BasePojo> call, Throwable t) {
                    // Log error here since request failed.
                    System.out.println("hh failure retrofit URL " + t.getMessage());
                    progressDialog.getlDialog().dismiss();
                }
            });
        } else {
            dialog.displayCommonDialogWithHeaderSmall(getResources().getString(R.string.internet_failed), WudStayConstants.NO_INTERNET_CONNECTED);
        }

    }

    private void showDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                myCalendar.set(year, month, day);
                int_year = year;
                int_month = month;
                int_day = day;
                updateLabel();
            }
        }, int_year,int_month, int_day);
        dialog.getDatePicker().setMinDate((System.currentTimeMillis() ) - 1000);
        dialog.show();
    }

    private void updateLabel() {
        SimpleDateFormat sdf = activity.getDateFormat();
        availableDate = sdf.format(myCalendar.getTime());
        pic_date_text.setText(availableDate);
    }

    @Override
    public void selectedIndices(List<Integer> indices) {
        String str = "";
        int[] ret = new int[indices.size()];
        for(int i = 0;i < ret.length;i++){
            ret[i] = indices.get(i);
            str += amenitiesValuesArr.get(ret[i])+",";

        }
        amenities = str.substring(0, (str.length()-1));
        System.out.println("xxxxxxxxxxxx amenities "+amenities);

    }

    @Override
    public void selectedStrings(List<String> strings) {
        Toast.makeText(getActivity(), strings.toString(), Toast.LENGTH_LONG).show();
    }

    public void loadAreaUnit()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("superBuildupAreaUnit");
            areaunitList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Log.d("Details-->", jo_inside.getString("name"));
                Log.d("Details-->", jo_inside.getString("value"));
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                areaunitList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(activity, areaunitList);
            super_build_spinner.setAdapter(state_adapter);
            build_up_area_spinner.setAdapter(state_adapter);
            carpet_area_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void loadBedRooms()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("bedrooms");
            bedroomList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                bedroomList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(activity, bedroomList);
            bedrooms_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void loadBathRooms()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("bathrooms");
            bathRoomList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                bathRoomList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(activity, bathRoomList);
            bathroom_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void loadBalkonies()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("balconies");
            balconiList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                balconiList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(activity, balconiList);
            balconies_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void loadFurnishing()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("furnishing");
            furnishingList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                furnishingList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(activity, furnishingList);
            furnishing_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void loadFloor()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("floors");
            floorList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                floorList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(activity, floorList);
            total_floors_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void loadPropertyFloor()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("property_floor");
            propertyFloorList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                propertyFloorList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(activity, propertyFloorList);
            property_floors_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void loadAmenities()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("amenities");
            amenitiesNamesArr = new ArrayList<>();
            amenitiesValuesArr = new ArrayList<>();

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                amenitiesValuesArr.add(i, url_value);
            }

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String formula_value = jo_inside.getString("name");

                //Add your values in your `ArrayList` as below:
                amenitiesNamesArr.add(i, formula_value);
            }
            if (amenitiesNamesArr != null) {
                amenities_Spinner.setItems(amenitiesNamesArr);
                amenities_Spinner.setListener(this);
            }
            amenities = amenitiesValuesArr.get(amenities_Spinner.getSelectedIndices().get(0));
            System.out.println("xxxxxxxxxxxx amenities "+amenities);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void loadSecurityDepositMode()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("security_deposit");
            securityDepositModeList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                securityDepositModeList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(activity, securityDepositModeList);
            security_deposit_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void loadPropertyAge()
    {

        try {
            JSONObject obj = new JSONObject(activity.loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("age_property");
            agePropertyList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String formula_value = jo_inside.getString("name");
                String url_value = jo_inside.getString("value");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("name", formula_value);
                m_li.put("value", url_value);

                agePropertyList.add(m_li);
            }
            state_adapter = new SpinnerListAdapter(activity, agePropertyList);
            property_age_spinner.setAdapter(state_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}