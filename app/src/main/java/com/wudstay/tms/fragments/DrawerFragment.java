package com.wudstay.tms.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.BaseActivity;
import com.wudstay.tms.activity.ComplaintsActivity;
import com.wudstay.tms.activity.CreateInvoiceActivity;
import com.wudstay.tms.activity.CreatePropertyActivity;
import com.wudstay.tms.activity.CreateReceiptActivity;
import com.wudstay.tms.activity.CreateTenantsActivity;
import com.wudstay.tms.activity.HomeActivity;
import com.wudstay.tms.activity.ProfileActivity;
import com.wudstay.tms.activity.SendNotificationActivity;
import com.wudstay.tms.activity.ViewPropertyActivity;
import com.wudstay.tms.activity.ViewTenantActivity;
import com.wudstay.tms.adapter.CustomExpandableListAdapter;
import com.wudstay.tms.utils.ConnectionDetector;
import com.wudstay.tms.utils.WudStayConstants;
import com.wudstay.tms.utils.WudstayDialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DrawerFragment extends Fragment {

    public ActionBarDrawerToggle mDrawerToggle;
    private int selectedPos = -1;
    private DrawerLayout drawerlayout;

    private ConnectionDetector cd;
    private WudstayDialogs dialog;

    private CustomExpandableListAdapter listAdapterProperty, listAdapterTenant, listAdapterPayment;
    private ExpandableListView property_expandable, tenant_expandable, payment_expandable;
    private List<String> propertyHeader, tenantHeader, paymentHeader;
    private HashMap<String, List<String>> propertyChild, tenantChild, paymentChild;
    private ImageView menu_profile_image;
    private TextView menu_edit_profile, menu_user_name, menu_notifications, menu_complaints, menu_logout, menu_dashboard;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.left_slide_list_layout, container, false);

        selectedPos = -1;

        cd = new ConnectionDetector(getActivity());
        dialog = new WudstayDialogs(getActivity());

        menu_notifications = (TextView) v.findViewById(R.id.menu_notifications);
        menu_user_name = (TextView) v.findViewById(R.id.menu_user_name);
        menu_edit_profile = (TextView) v.findViewById(R.id.menu_edit_profile);
        menu_complaints = (TextView) v.findViewById(R.id.menu_complaints);
        menu_dashboard = (TextView) v.findViewById(R.id.menu_dashboard);
        menu_logout = (TextView) v.findViewById(R.id.menu_logout);
        menu_profile_image = (ImageView) v.findViewById(R.id.menu_profile_image);

        setClickListener(menu_dashboard, 0);
        setClickListener(menu_notifications, 1);
        setClickListener(menu_complaints, 2);
        setClickListener(menu_logout, 3);

        // get the listview
        property_expandable = (ExpandableListView) v.findViewById(R.id.property_expandable);
        tenant_expandable = (ExpandableListView) v.findViewById(R.id.tenant_expandable);
        payment_expandable = (ExpandableListView) v.findViewById(R.id.payment_expandable);

        // preparing list data
        prepareListData();

        listAdapterProperty = new CustomExpandableListAdapter(getActivity(), propertyHeader, propertyChild);
        property_expandable.setAdapter(listAdapterProperty);
        listAdapterTenant = new CustomExpandableListAdapter(getActivity(), tenantHeader, tenantChild);
        tenant_expandable.setAdapter(listAdapterTenant);
        listAdapterPayment = new CustomExpandableListAdapter(getActivity(), paymentHeader, paymentChild);
        payment_expandable.setAdapter(listAdapterPayment);

        if (getFromPrefs(WudStayConstants.USER_NAME) != null && !getFromPrefs(WudStayConstants.USER_NAME).equals("")) {
//                    logout.setVisibility(View.VISIBLE);
            menu_user_name.setText(getFromPrefs(WudStayConstants.USER_NAME));
            menu_edit_profile.setText("Edit Profile");
            if (getFromPrefs("image") != null && !getFromPrefs("image").equals(""))
                ((BaseActivity) getActivity()).setProfileImageInLayout(getActivity(), (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image),
                        getFromPrefs("image"), menu_profile_image);
        } else {
//                    logout.setVisibility(View.GONE);
            menu_user_name.setText(getString(R.string.dear_user));
            menu_edit_profile.setText("Register/Login");
        }

        expendListView(property_expandable);
        expendListView(tenant_expandable);
        expendListView(payment_expandable);
        // Listview on child click listener
        property_expandable.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                ComponentName componentName = getActivity().getComponentName();
                drawerlayout.closeDrawers();
                setListViewHeight(parent, groupPosition);
                if (childPosition == 0 && !componentName.getShortClassName().equals(".activity.CreatePropertyActivity")) {
                    navigateToCreateProperty();
                } else if (childPosition == 1 && !componentName.getShortClassName().equals(".activity.ViewPropertyActivity")) {
                    navigateToViewProperty();
                }
                parent.collapseGroup(groupPosition);
                return false;
            }
        });

        // Listview on child click listener
        tenant_expandable.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                ComponentName componentName = getActivity().getComponentName();
                drawerlayout.closeDrawers();
                setListViewHeight(parent, groupPosition);
                if (childPosition == 0 && !componentName.getShortClassName().equals(".activity.CreateTenantsActivity")) {
                    navigateToCreateTenant();
                } else if (childPosition == 1 && !componentName.getShortClassName().equals(".activity.ViewTenantActivity")) {
                    navigateToViewTenant();
                }
                parent.collapseGroup(groupPosition);
                return false;
            }
        });

        // Listview on child click listener
        payment_expandable.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                ComponentName componentName = getActivity().getComponentName();
                drawerlayout.closeDrawers();
                setListViewHeight(parent, groupPosition);
                if (childPosition == 0 && !componentName.getShortClassName().equals(".activity.CreateInvoiceActivity")) {
                    navigateToCreateInvoice();
                } else if (childPosition == 1 && !componentName.getShortClassName().equals(".activity.CreateReceiptActivity")) {
                    navigateToCreateReceipt();
                }
                parent.collapseGroup(groupPosition);
                return false;
            }
        });

        return v;
    }

    private void setClickListener(final TextView layout, final int pos) {
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        selectedPos = pos;
                        drawerlayout.closeDrawers();
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        selectedPos = -1;
                        break;
                }
                return true;
            }
        });
    }

    @SuppressLint("NewApi")
    public void setUp(final DrawerLayout drawerlayout) {

        this.drawerlayout = drawerlayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerlayout, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
                ((BaseActivity) getActivity()).hideSoftKeyboard();
                menu_edit_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        navigateToProfile();
                    }
                });
                if (getFromPrefs(WudStayConstants.USER_NAME) != null && !getFromPrefs(WudStayConstants.USER_NAME).equals("")) {
//                    logout.setVisibility(View.VISIBLE);
                    menu_user_name.setText(getFromPrefs(WudStayConstants.USER_NAME));
                    menu_edit_profile.setText("Edit Profile");
                    if (getFromPrefs("image") != null && !getFromPrefs("image").equals(""))
                        ((BaseActivity) getActivity()).setProfileImageInLayout(getActivity(), (int) getResources().getDimension(R.dimen.profile_image), (int) getResources().getDimension(R.dimen.profile_image),
                                getFromPrefs("image"), menu_profile_image);
                } else {
//                    logout.setVisibility(View.GONE);
                    menu_user_name.setText(getString(R.string.dear_user));
                    menu_edit_profile.setText("Register/Login");
                }
            }

            @SuppressLint("NewApi")
            @Override
            public void onDrawerClosed(View drawerView) {
                ActivityManager am = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                super.onDrawerClosed(drawerView);
                if (selectedPos != -1) {
                    switch (selectedPos) {
                        case 0:
                            if (!cn.getShortClassName().equals(".activity.HomeActivity")) {
                                navigateToDashBoard();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;

                        case 1:
                            if (!cn.getShortClassName().equals(".activity.SendNotificationActivity")) {
                                navigateToNotifications();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;

                        case 2:
                            if (!cn.getShortClassName().equals(".activity.ComplaintsActivity")) {
                                navigateToComplaints();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;

                        case 3:

                            logOut();
                            break;

                        default:
                            break;
                    }
                    selectedPos = -1;
                }
            }
        };

        drawerlayout.setDrawerListener(mDrawerToggle);

    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getActivity().getSharedPreferences(WudStayConstants.PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, WudStayConstants.DEFAULT_VALUE);
    }

    private void navigateToProfile() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".ProfileActivity");
        Intent mIntent = new Intent(getActivity(), ProfileActivity.class);
        startActivity(mIntent);
    }

    private void navigateToComplaints() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".ComplaintsActivity");
        Intent mIntent = new Intent(getActivity(), ComplaintsActivity.class);
        startActivity(mIntent);
    }

    private void navigateToDashBoard() {
        drawerlayout.closeDrawers();
        for (int i = 0; i < WudStayConstants.ACTIVITIES.size(); i++) {
            if (WudStayConstants.ACTIVITIES.get(i) != null)
                WudStayConstants.ACTIVITIES.get(i).finish();
        }
        getActivity().finish();
        removeActivity(getResources().getString(R.string.package_name) + ".HomeActivity");
        Intent mIntent = new Intent(getActivity(), HomeActivity.class);
        startActivity(mIntent);
    }

    private void navigateToNotifications() {
        drawerlayout.closeDrawers();
        removeActivity(getResources().getString(R.string.package_name) + ".SendNotificationActivity");
        Intent mIntent = new Intent(getActivity(), SendNotificationActivity.class);
        startActivity(mIntent);
    }

    private void navigateToCreateProperty() {
        removeActivity(getResources().getString(R.string.package_name) + ".CreatePropertyActivity");
        Intent mIntent = new Intent(getActivity(), CreatePropertyActivity.class);
        startActivity(mIntent);
    }

    private void navigateToCreateInvoice() {
        removeActivity(getResources().getString(R.string.package_name) + ".CreateInvoiceActivity");
        Intent mIntent = new Intent(getActivity(), CreateInvoiceActivity.class);
        startActivity(mIntent);
    }

    private void navigateToCreateTenant() {
        removeActivity(getResources().getString(R.string.package_name) + ".CreateTenantsActivity");
        Intent mIntent = new Intent(getActivity(), CreateTenantsActivity.class);
        startActivity(mIntent);
    }

    private void navigateToViewProperty() {
        removeActivity(getResources().getString(R.string.package_name) + ".ViewPropertyActivity");
        Intent mIntent = new Intent(getActivity(), ViewPropertyActivity.class);
        startActivity(mIntent);
    }

    private void navigateToViewTenant() {
        removeActivity(getResources().getString(R.string.package_name) + ".ViewTenantActivity");
        Intent mIntent = new Intent(getActivity(), ViewTenantActivity.class);
        startActivity(mIntent);
    }

    private void navigateToCreateReceipt() {
        removeActivity(getResources().getString(R.string.package_name) + ".CreateReceiptActivity");
        Intent mIntent = new Intent(getActivity(), CreateReceiptActivity.class);
        startActivity(mIntent);
    }

    private void logOut()
    {
        drawerlayout.closeDrawers();
        WudstayDialogs logout_dialogs = new WudstayDialogs(getActivity());
        logout_dialogs.displayDialogWithCancel("Are you sure you want to Logout");

    }

    private void removeActivity(String activity) {
        for (int i = 0; i < WudStayConstants.ACTIVITIES.size(); i++) {
            if (WudStayConstants.ACTIVITIES.get(i) != null && WudStayConstants.ACTIVITIES.get(i).toString().contains(activity)) {
                WudStayConstants.ACTIVITIES.get(i).finish();
                WudStayConstants.ACTIVITIES.remove(i);
                break;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        propertyHeader = new ArrayList<>();
        tenantHeader = new ArrayList<>();
        paymentHeader = new ArrayList<>();
        propertyChild = new HashMap<>();
        tenantChild = new HashMap<>();
        paymentChild = new HashMap<>();

        // Adding child data
        propertyHeader.add("Property");
        tenantHeader.add("Tenant");
        paymentHeader.add("Payment");

        // Adding child data
        List<String> property = new ArrayList<>();
        property.add("Create");
        property.add("View");


        List<String> tenant = new ArrayList<>();
        tenant.add("Create");
        tenant.add("View");

        List<String> payment = new ArrayList<>();
        payment.add("Invoice");
        payment.add("Receipt");

        propertyChild.put(propertyHeader.get(0), property); // Header, Child data
        tenantChild.put(tenantHeader.get(0), tenant);
        paymentChild.put(paymentHeader.get(0), payment);
    }

    private void setListViewHeight(ExpandableListView listView,
                                   int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    private void expendListView(ExpandableListView expendableList)
    {
        expendableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                setListViewHeight(parent, groupPosition);
                return false;
            }
        });
    }

}
