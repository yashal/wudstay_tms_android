package com.wudstay.tms.model;

import com.google.gson.JsonObject;
import com.wudstay.tms.pojo.BasePojo;
import com.wudstay.tms.pojo.ComplainPojo;
import com.wudstay.tms.pojo.CreateTPojo;
import com.wudstay.tms.pojo.DashboardPojo;
import com.wudstay.tms.pojo.GetPropertyPojo;
import com.wudstay.tms.pojo.GetTokenPojo;
import com.wudstay.tms.pojo.GetUserTyepPojo;
import com.wudstay.tms.pojo.InvoicePojo;
import com.wudstay.tms.pojo.ListTenantPojo;
import com.wudstay.tms.pojo.LoginPojo;
import com.wudstay.tms.pojo.OtherBasePojo;
import com.wudstay.tms.pojo.PaymentReceiptPojo;
import com.wudstay.tms.pojo.PropertyPojo;
import com.wudstay.tms.pojo.TenantListPojo;
import com.wudstay.tms.pojo.TenantParticularPojo;
import com.wudstay.tms.pojo.TenantPropertyListPojo;
import com.wudstay.tms.pojo.ValidateUserPojo;
import com.wudstay.tms.pojo.ViewDocPojo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("mobile/gettoken")
    Call<GetTokenPojo> getToken(@Header("Cookie") String header);

    @GET("mobile/getusertype")
    Call<GetUserTyepPojo> getUserType(@Header("Cookie") String header);

    @FormUrlEncoded
    @POST("mobile/login")
    Call<LoginPojo> login(@Header("Cookie") String header, @Field("email") String email, @Field("password") String password, @Field("_csrf") String _csrf);

    @GET("mobile/dashboard")
    Call<DashboardPojo> getDashBoard(@Header("Cookie") String header);

    @GET("validatenewuser")
    Call<ValidateUserPojo> validateNewUser(@Header("Cookie") String header, @Query("name") String name, @Query("mobile") String mobile,
              @Query("email") String email, @Query("password") String password, @Query("confirmPassword") String confirmPassword);

    @GET("registernewuser")
    Call<BasePojo> registerNewUser(@Header("Cookie") String header, @Query("name") String name, @Query("mobile") String mobile,
                                            @Query("email") String email, @Query("password") String password,
                                           @Query("confirmPassword") String confirmPassword, @Query("mobileotp") String mobileotp
                                            , @Query("usertype") String usertype);

    @GET("mobile/logout")
    Call<BasePojo> logOut(@Header("Cookie") String header);

    @GET("mobile/propertylist")
    Call<PropertyPojo> propertyList(@Header("Cookie") String header);

    @GET("mobile/property")
    Call<GetPropertyPojo> getProperty(@Header("Cookie") String header);

    @GET("mobile/listingonwoudstay")
    Call<PropertyPojo> listingOnWudstay(@Header("Cookie") String header, @Query("propertyref") String propertyref);


    @FormUrlEncoded
    @POST("mobile/requestforlisting")
    Call<BasePojo> requestForListing(@Header("Cookie") String header, @Field("_csrf") String _csrf, @Field("superBuildUpArea") String superBuildUpArea, @Field("superBuildAreaUnit") String superBuildAreaUnit,
                                     @Field("buildUpArea") String buildUpArea, @Field("buildUpAreaUnit") String buildUpAreaUnit, @Field("carpetArea") String carpetArea, @Field("carpetAreaUnit") String carpetAreaUnit,
                                     @Field("bedRooms") String bedRooms, @Field("bathrooms") String bathrooms, @Field("balconies") String balconies, @Field("furnishingType") String furnishingType,
                                     @Field("totalFloors") String totalFloors, @Field("propertyOnFloor") String propertyOnFloor, @Field("availableDate") String availableDate, @Field("expectedMonthlyRent") String expectedMonthlyRent,
                                     @Field("securityDeposit") String securityDeposit, @Field("ageOfProperty") String ageOfProperty, @Field("securityDepositMode") String securityDepositMode, @Field("propertyRef") String propertyRef,
                                     @Field("amenities") String amenities);


    @GET("mobile/tenantlist")
    Call<TenantPropertyListPojo> getTenantPropertyList(@Header("Cookie") String header);

    @GET("getTenants")
    Call<TenantListPojo> getTenantList(@Header("Cookie") String header, @Query("propertyId") String propertyId);

    @GET("mobile/tenant")
    Call<ListTenantPojo> getTenant(@Header("Cookie") String header);

    @GET("mobile/imageview")
    Call<ViewDocPojo> viewDocs(@Header("Cookie") String header, @Query("id") String id);

    @GET("getTenantsWithParticulars")
    Call<TenantParticularPojo> getTenantsWithParticulars(@Header("Cookie") String header, @Query("propertyId") String propertyId);

    @Headers("Content-Type: application/json")
    @POST("createT")
    Call<CreateTPojo> createT(@Header("Cookie") String header, @Header("X-CSRF-TOKEN") String token, @Body JsonObject invoiceParticularForTenantDtos);

    @GET("mobile/invoice")
    Call<InvoicePojo> getInvoice(@Header("Cookie") String header);

    @Headers("Content-Type: application/json")
    @POST("invo/createParticulars")
    Call<OtherBasePojo> createParticulars(@Header("Cookie") String header, @Header("X-CSRF-TOKEN") String token, @Body JsonObject invoiceParticularForTenantDtos);

    @Headers("Content-Type: application/json")
    @POST("invo/createInvoice")
    Call<OtherBasePojo> createInvoice(@Header("Cookie") String header, @Header("X-CSRF-TOKEN") String token, @Body JsonObject tenantIds);

    @GET("mobile/paymentreceipt")
    Call<PaymentReceiptPojo> getPaymentReceipt(@Header("Cookie") String header);

    @GET("invo/getMonthlyPayments")
    Call<TenantParticularPojo> getMonthlyPayments(@Header("Cookie") String header,  @Query("propertyId") String propertyId);

    @GET("mobile/viewtenantcomplaints")
    Call<ComplainPojo> getTenantComplaints(@Header("Cookie") String header);
}
