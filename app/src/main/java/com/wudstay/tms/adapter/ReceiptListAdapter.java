package com.wudstay.tms.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.CreateInvoiceActivity;
import com.wudstay.tms.activity.CreateReceiptActivity;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class ReceiptListAdapter extends RecyclerView.Adapter<ReceiptListAdapter.ViewHolder> {

    private int height, width;

    private CreateReceiptActivity context;

    public ReceiptListAdapter(CreateReceiptActivity context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.itemView.setOnClickListener(context);
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView pgLocation, pgRent, tenant_name, pgName;
        private ImageView individual_select, tenant_image;

        public ViewHolder(View itemView) {
            super(itemView);
            pgLocation = (TextView) itemView.findViewById(R.id.pgLocation);
            pgRent = (TextView) itemView.findViewById(R.id.pgRent);
            tenant_name = (TextView) itemView.findViewById(R.id.tenant_name);
            pgName = (TextView) itemView.findViewById(R.id.pgName);

            individual_select = (ImageView) itemView.findViewById(R.id.individual_select);
            tenant_image = (ImageView) itemView.findViewById(R.id.tenant_image);
        }
    }
}
