package com.wudstay.tms.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.PropertyListDetailsActivity;
import com.wudstay.tms.activity.RequestListingActivity;
import com.wudstay.tms.activity.ViewPropertyActivity;
import com.wudstay.tms.pojo.OccupancyWiseRoomsPojo;
import com.wudstay.tms.pojo.PropertiesListPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class OccupancyAdapter extends RecyclerView.Adapter<OccupancyAdapter.ViewHolder> {

    private int height, width;
    private ArrayList<OccupancyWiseRoomsPojo> arrOccupancyList;

    private PropertyListDetailsActivity context;
    private RequestListingActivity ctx;
    private String from;

    public OccupancyAdapter(Activity context, ArrayList<OccupancyWiseRoomsPojo> arrOccupancyList, String from) {
        this.from = from;
        if (from.equals("PropertyListDetailsActivity"))
        {
            this.context =(PropertyListDetailsActivity)context;
        }
        else if (from.equals("RequestListingActivity"))
        {
            this.ctx =(RequestListingActivity)context;
        }

        this.arrOccupancyList = arrOccupancyList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.occupancy_list_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final OccupancyWiseRoomsPojo data  = arrOccupancyList.get(position);
        holder.serial_no.setText(""+(position+1));
        if (data.getOccupancyName() != null && !data.getOccupancyName().isEmpty())
        {
            holder.occupancy_type.setText(data.getOccupancyName() );
        }
        if (data.getNoOfBeds() != null)
        {
            holder.bed_counts.setText(""+data.getNoOfBeds() );
        }
        if (data.getNoOfBeds() != null)
        {
            holder.bed_counts.setText(""+data.getNoOfBeds() );
        }
        if (data.getNoOfRooms() != null)
        {
            holder.room_counts.setText(""+data.getNoOfRooms() );
        }
    }

    @Override
    public int getItemCount() {
        return arrOccupancyList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView occupancy_type, serial_no, bed_counts, room_counts;

        public ViewHolder(View itemView) {
            super(itemView);
            occupancy_type = (TextView) itemView.findViewById(R.id.occupancy_type);
            serial_no = (TextView) itemView.findViewById(R.id.serial_no);
            bed_counts = (TextView) itemView.findViewById(R.id.bed_counts);
            room_counts = (TextView) itemView.findViewById(R.id.room_counts);

        }
    }
}
