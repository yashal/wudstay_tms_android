package com.wudstay.tms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.RequestListingActivity;
import com.wudstay.tms.activity.ViewDocs;
import com.wudstay.tms.pojo.ViewDocTenantPojo;

import java.util.ArrayList;

/**
 * Created by Yash on 4/3/2017.
 */

public class ViewDocsAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ViewDocTenantPojo> tenantDocs;

    public ViewDocsAdapter(Context c, ArrayList<ViewDocTenantPojo> tenantDocs) {
        mContext = c;
        this.tenantDocs = tenantDocs;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return (tenantDocs.size());
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ViewHolder holder;
        if (convertView == null) {

            //  convertView = new View(mContext);
            convertView = inflater.inflate(R.layout.grid_view_docs, null);
            holder = new ViewHolder();
            holder.property_image = (ImageView) convertView.findViewById(R.id.grid_image);


        } else {
            holder = (ViewHolder) convertView.getTag();

        }

        System.out.println("xxxx yashal " + tenantDocs.get(position).getUrl());
        ((ViewDocs) mContext).setImageInLayout(mContext, 80, 150, tenantDocs.get(position).getUrl(), holder.property_image);

        convertView.setTag(holder);


        return convertView;
    }

    // convert view class
    class ViewHolder {
        ImageView property_image;
    }
}
