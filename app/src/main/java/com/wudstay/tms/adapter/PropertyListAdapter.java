package com.wudstay.tms.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.HomeActivity;

import java.util.ArrayList;


public class PropertyListAdapter extends PagerAdapter {
	// Declare Variables
	HomeActivity context;
//	ArrayList<String> gallery_arr;
	LayoutInflater inflater;
	int width;

	public PropertyListAdapter(HomeActivity context, /*ArrayList<String> gallery_arr,*/ int width) {
		this.context = context;
//		this.gallery_arr = gallery_arr;
		this.width = width;
	}

	@Override
	public int getCount() {
		return 5;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		// Declare Variables
		ImageView imgflag;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.gallery_layout, container, false);

		// Locate the ImageView in viewpager_item.xml
		imgflag = (ImageView) itemView.findViewById(R.id.flag);
		// Capture position and set to the ImageView
//		imgflag.setBackgroundResource(R.mipmap.splash);
//		context.setImageInLayout(context, width, 600, gallery_arr.get(position), imgflag);

		// Add viewpager_item.xml to ViewPager
		((ViewPager) container).addView(itemView);

		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		((ViewPager) container).removeView((RelativeLayout) object);

	}

}