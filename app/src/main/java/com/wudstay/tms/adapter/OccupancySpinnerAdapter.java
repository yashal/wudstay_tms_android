package com.wudstay.tms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.pojo.OccupancyList;
import com.wudstay.tms.pojo.PropertiesListPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/12/2016.
 */

public class OccupancySpinnerAdapter extends BaseAdapter {

    private ArrayList<OccupancyList> occupancyList;
    private Context context;

    public OccupancySpinnerAdapter(Context context, ArrayList<OccupancyList> occupancyList){
        this.context = context;
        this.occupancyList = occupancyList;
    }

    @Override
    public int getCount() {
        return occupancyList.size();
    }

    @Override
    public Object getItem(int i) {
        return occupancyList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.user_row_layout, null);

        TextView tv_state = (TextView) convertView.findViewById(R.id.tv_state);


            tv_state.setText(occupancyList.get(position).getOccupancyName());

           return convertView;

    }

}
