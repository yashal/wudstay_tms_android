package com.wudstay.tms.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.CreatePropertyActivity;
import com.wudstay.tms.activity.PropertyListDetailsActivity;
import com.wudstay.tms.pojo.OccupancyWiseNoOfRooms;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class FlatAdapter extends RecyclerView.Adapter<FlatAdapter.ViewHolder> {

    private int height, width;
    private ArrayList<OccupancyWiseNoOfRooms>  flatType;


    private PropertyListDetailsActivity context;
    private CreatePropertyActivity ctx;
    private String from;

    public FlatAdapter(Activity context, ArrayList<OccupancyWiseNoOfRooms>  pgType) {

            this.ctx =(CreatePropertyActivity)context;

        this.flatType = pgType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.flat_list_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final OccupancyWiseNoOfRooms data  = flatType.get(position);
        if (data.getOccupancyName() != null && !data.getOccupancyName().isEmpty())
        {
            holder.flat_type.setText(data.getOccupancyName() );
        }


    }

    @Override
    public int getItemCount() {
        return flatType.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView flat_type;
        private ImageView redio_select;

        public ViewHolder(View itemView) {
            super(itemView);
            flat_type = (TextView) itemView.findViewById(R.id.flat_type);
            redio_select = (ImageView) itemView.findViewById(R.id.radioButton);

        }
    }
}
