package com.wudstay.tms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.RequestListingActivity;
import com.wudstay.tms.pojo.UserTypeList;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by quepplin1 on 8/12/2016.
 */

public class SpinnerListAdapter extends BaseAdapter {

    private ArrayList<HashMap<String, String>> formList;
    private Context context;

    public SpinnerListAdapter(Context context, ArrayList<HashMap<String, String>> formList){
        this.context = context;
        this.formList = formList;
    }

    @Override
    public int getCount() {
        return formList.size();
    }

    @Override
    public Object getItem(int i) {
        return formList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.spinner_row_layout, null);

        TextView tv_state = (TextView) convertView.findViewById(R.id.tv_state);


            tv_state.setText(formList.get(position).get("name"));

           return convertView;

    }

}
