package com.wudstay.tms.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.ComplaintsActivity;
import com.wudstay.tms.activity.CreateInvoiceActivity;
import com.wudstay.tms.pojo.OwnerLsitProperties;
import com.wudstay.tms.pojo.PropertiesListPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class ComplaintListAdapter extends RecyclerView.Adapter<ComplaintListAdapter.ViewHolder> {

    private ComplaintsActivity context;
    private ArrayList<OwnerLsitProperties> listOfOwnProperties;

    public ComplaintListAdapter(ComplaintsActivity context, ArrayList<OwnerLsitProperties> listOfOwnProperties) {
        this.context = context;
        this.listOfOwnProperties = listOfOwnProperties;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.complaint_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final OwnerLsitProperties data = listOfOwnProperties.get(position);

        if (data.getTenantOrUser().getTenantName() != null && !data.getTenantOrUser().getTenantName().isEmpty())
        {
            holder.tenant_name.setText(data.getTenantOrUser().getTenantName());
        }
        if (data.getPropertyDto().getPropertyName() != null && !data.getPropertyDto().getPropertyName().isEmpty())
        {
            holder.pgName.setText(data.getPropertyDto().getPropertyName());
        }
        if (data.getComplaintType() != null && !data.getComplaintType().isEmpty())
        {
            holder.issue.setText("Complaint Type: "+data.getComplaintType());
        }
        if (data.getLatestStatus() != null && !data.getLatestStatus().isEmpty())
        {
            holder.status.setText("Status: "+data.getLatestStatus());
        }

        holder.itemView.setTag(R.string.key, data);
        holder.itemView.setOnClickListener(context);
    }

    @Override
    public int getItemCount() {
        return listOfOwnProperties.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView issue, status, tenant_name, pgName;

        public ViewHolder(View itemView) {
            super(itemView);
            issue = (TextView) itemView.findViewById(R.id.issue);
            status = (TextView) itemView.findViewById(R.id.status);
            tenant_name = (TextView) itemView.findViewById(R.id.tenant_name);
            pgName = (TextView) itemView.findViewById(R.id.pgName);
        }
    }
}
