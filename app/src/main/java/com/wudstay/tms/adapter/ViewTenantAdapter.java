package com.wudstay.tms.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wudstay.tms.R;
import com.wudstay.tms.activity.ViewTenantActivity;
import com.wudstay.tms.pojo.PropertiesListPojo;
import com.wudstay.tms.pojo.TenantDtoListPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class ViewTenantAdapter extends RecyclerView.Adapter<ViewTenantAdapter.ViewHolder> {

    private int height, width;
    private ArrayList<TenantDtoListPojo> tenantDtoList;

    private ViewTenantActivity context;

    public ViewTenantAdapter(ViewTenantActivity context, ArrayList<TenantDtoListPojo> tenantDtoList) {
        this.context = context;
        this.tenantDtoList = tenantDtoList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tenants_list_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final TenantDtoListPojo data = tenantDtoList.get(position);

        if (data.getTenantName() != null && !data.getTenantName().isEmpty())
        {
            holder.tenant_name.setText(data.getTenantName());
        }
        if (data.getTenantEmail() != null && !data.getTenantEmail().isEmpty())
        {
            holder.tenant_email.setText(data.getTenantEmail());
        }
        if (data.getTenantMobile() != null && !data.getTenantMobile().isEmpty())
        {
            holder.tenant_mobile.setText(data.getTenantMobile());
        }

        holder.view_docs.setTag(R.string.key,data.getId());
        holder.view_docs.setOnClickListener(context);
    }

    @Override
    public int getItemCount() {
        return tenantDtoList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tenant_name, tenant_email, tenant_mobile;
        private LinearLayout send_view_agreement, tenant_move_out, view_docs;

        public ViewHolder(View itemView) {
            super(itemView);
            tenant_name = (TextView) itemView.findViewById(R.id.tenant_name);
            tenant_email = (TextView) itemView.findViewById(R.id.tenant_email);
            tenant_mobile = (TextView) itemView.findViewById(R.id.tenant_mobile);
            send_view_agreement = (LinearLayout) itemView.findViewById(R.id.send_view_agreement);
            tenant_move_out = (LinearLayout) itemView.findViewById(R.id.tenant_move_out);
            view_docs = (LinearLayout) itemView.findViewById(R.id.view_docs);

        }
    }
}
