package com.wudstay.tms.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.CreatePropertyActivity;
import com.wudstay.tms.activity.CreateTenantsActivity;
import com.wudstay.tms.activity.PropertyListDetailsActivity;
import com.wudstay.tms.pojo.OccupancyWiseNoOfRooms;
import com.wudstay.tms.pojo.ParticularList;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class ParticularAdapter extends RecyclerView.Adapter<ParticularAdapter.ViewHolder> {

    private int height, width;
    private ArrayList<ParticularList> particularsList;


    private PropertyListDetailsActivity context;
    private CreateTenantsActivity ctx;
    private String from;

    public ParticularAdapter(Activity context, ArrayList<ParticularList> particularsList) {

            this.ctx =(CreateTenantsActivity)context;

        this.particularsList = particularsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.particular_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final ParticularList data  = particularsList.get(position);

        if (data.getParticularName()!= null && !data.getParticularName().isEmpty())
        {
            if (data.getParticularName().equalsIgnoreCase("Room Rent") || data.getParticularName().equalsIgnoreCase("Security"))
            {
                holder.particular_name.setText(data.getParticularName()+"*");
            }
            else
            {
                holder.particular_name.setText(data.getParticularName());
            }

        }
        if (data.getType()!= null && !data.getType().isEmpty())
        {
            if (data.getType().equalsIgnoreCase("ONE TIME"))
            {
                holder.type.setText("(ONE TIME)");
            }
            else
            {
                holder.type.setText("(charges per month)");
            }
        }

        holder.amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                data.setAmount(holder.amount.getText().toString());
            }
        });

    }

    @Override
    public int getItemCount() {
        return particularsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView particular_name, type;
        private EditText amount;

        public ViewHolder(View itemView) {
            super(itemView);
            particular_name = (TextView) itemView.findViewById(R.id.particular_name);
            type = (TextView) itemView.findViewById(R.id.type);
            amount = (EditText) itemView.findViewById(R.id.amount);

        }
    }
}
