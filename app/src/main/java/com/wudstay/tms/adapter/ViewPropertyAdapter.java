package com.wudstay.tms.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.ViewPropertyActivity;
import com.wudstay.tms.pojo.PropertiesListPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class ViewPropertyAdapter extends RecyclerView.Adapter<ViewPropertyAdapter.ViewHolder> {

    private int height, width;
    private ArrayList<PropertiesListPojo> arrPropertyList;

    private ViewPropertyActivity context;

    public ViewPropertyAdapter(ViewPropertyActivity context, ArrayList<PropertiesListPojo> arrPropertyList) {
        this.context = context;
        this.arrPropertyList = arrPropertyList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.property_list_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final PropertiesListPojo data = arrPropertyList.get(position);

        if (data.getPropertyAddress() != null && !data.getPropertyAddress().isEmpty()){
            holder.property_address.setText(data.getPropertyAddress());
        }
        if (data.getPincode() != null && !data.getPincode().isEmpty()){
            holder.pin_code.setText(data.getPincode());
        }
        if (data.getPropertyTypeName() != null && !data.getPropertyTypeName().isEmpty()){
            holder.property_type.setText(data.getPropertyTypeName());
        }
        if (data.getCityName() != null && !data.getCityName().isEmpty()){
            holder.city_name.setText(data.getCityName());
        }
        if (data.getPropertyName() != null && !data.getPropertyName().isEmpty()){
            holder.property_name.setText(data.getPropertyName());
        }
        holder.itemView.setTag(R.string.key, data);
        holder.request_listing.setTag(R.string.key, data);

        if (data.getListingStatus() == 0)
        {
            holder.request_listing.setVisibility(View.VISIBLE);
        }
        else {
            holder.request_listing.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(context);
        holder.request_listing.setOnClickListener(context);
    }

    @Override
    public int getItemCount() {
        return arrPropertyList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView property_address, pin_code, property_type, city_name, request_listing, property_name;

        public ViewHolder(View itemView) {
            super(itemView);
            property_address = (TextView) itemView.findViewById(R.id.property_address);
            pin_code = (TextView) itemView.findViewById(R.id.pin_code);
            property_type = (TextView) itemView.findViewById(R.id.property_type);
            city_name = (TextView) itemView.findViewById(R.id.city_name);
            request_listing = (TextView) itemView.findViewById(R.id.request_listing);
            property_name = (TextView) itemView.findViewById(R.id.property_name);

        }
    }
}
