package com.wudstay.tms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.pojo.CityListPojo;
import com.wudstay.tms.pojo.UserTypeList;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/12/2016.
 */

public class CityListAdapter extends BaseAdapter {

    private ArrayList<CityListPojo> ListOfCities;
    private Context context;

    public CityListAdapter(Context context, ArrayList<CityListPojo> ListOfCities){
        this.context = context;
        this.ListOfCities = ListOfCities;
    }

    @Override
    public int getCount() {
        return ListOfCities.size();
    }

    @Override
    public Object getItem(int i) {
        return ListOfCities.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.user_row_layout, null);

        TextView tv_state = (TextView) convertView.findViewById(R.id.tv_state);


            tv_state.setText(ListOfCities.get(position).getValue());

           return convertView;

    }

}
