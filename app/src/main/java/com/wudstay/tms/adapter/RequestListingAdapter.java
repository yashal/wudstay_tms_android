package com.wudstay.tms.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wudstay.tms.fragments.DetailsFragment;
import com.wudstay.tms.fragments.ImageUploadFragment;
import com.wudstay.tms.fragments.OtherDetailsFragment;

/**
 * Created by Yash on 4/3/2017.
 */

public class RequestListingAdapter extends FragmentPagerAdapter {

    public RequestListingAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new DetailsFragment();
            case 1:
                return new ImageUploadFragment();
            case 2:
                return new OtherDetailsFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }
}

