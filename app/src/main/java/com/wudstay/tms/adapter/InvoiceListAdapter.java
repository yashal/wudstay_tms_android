package com.wudstay.tms.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.CreateInvoiceActivity;
import com.wudstay.tms.activity.CreateReceiptActivity;
import com.wudstay.tms.pojo.TenantDtoListPojo;
import com.wudstay.tms.pojo.TenantParticularDataPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class InvoiceListAdapter extends RecyclerView.Adapter<InvoiceListAdapter.ViewHolder> {

    private int height, width;

    private Context context;
    private ArrayList<TenantParticularDataPojo> arrList;
    private String from;

    public InvoiceListAdapter(Context context, ArrayList<TenantParticularDataPojo> arrList, String from) {
        this.context = context;
        this.arrList = arrList;
        this.from = from;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final TenantParticularDataPojo data = arrList.get(position);

        if (data.getTenantName() != null && !data.getTenantName().isEmpty())
        {
            holder.tenant_name.setText(data.getTenantName());
        }

        if (data.getTenantEmail() != null && !data.getTenantEmail().isEmpty())
        {
            holder.tenant_email.setText(data.getTenantEmail());
        }

        if (data.getTenantMobile() != null && !data.getTenantMobile().isEmpty())
        {
            holder.tenant_mobile.setText(data.getTenantMobile());
        }

        if (data.isStatus())
        {
            holder.individual_select.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.checked_box));
        }
        else
        {
            holder.individual_select.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.check_box));
        }

        holder.itemView.setTag(R.string.data,data);

        holder.individual_select.setTag(R.string.data, arrList);
        holder.individual_select.setTag(R.string.key, position);

        if (from.equalsIgnoreCase("Invoice"))
        {
            holder.pgRent.setText(context.getResources().getString(R.string.Rs)+" "+data.getRoomAmount());
            holder.itemView.setOnClickListener((CreateInvoiceActivity)context);
            holder.individual_select.setOnClickListener((CreateInvoiceActivity)context);
        }
        else if(from.equalsIgnoreCase("Receipt"))
        {
            holder.pgRent.setText(((CreateReceiptActivity)context).getResources().getString(R.string.Rs)+" "+data.getBalance());
            holder.itemView.setOnClickListener((CreateReceiptActivity)context);
            holder.individual_select.setOnClickListener((CreateReceiptActivity)context);
        }

    }

    @Override
    public int getItemCount() {
        return arrList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tenant_email, pgRent, tenant_name, tenant_mobile;
        private ImageView individual_select;

        public ViewHolder(View itemView) {
            super(itemView);
            tenant_email = (TextView) itemView.findViewById(R.id.tenant_email);
            pgRent = (TextView) itemView.findViewById(R.id.pgRent);
            tenant_name = (TextView) itemView.findViewById(R.id.tenant_name);
            tenant_mobile = (TextView) itemView.findViewById(R.id.tenant_mobile);

            individual_select = (ImageView) itemView.findViewById(R.id.individual_select);
        }
    }
}
