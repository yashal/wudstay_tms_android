package com.wudstay.tms.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wudstay.tms.R;
import com.wudstay.tms.activity.RequestListingActivity;

import java.util.ArrayList;

/**
 * Created by Yash on 4/3/2017.
 */

 public class UploadImageAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<String> listofImage;
    private String image_url;

    public UploadImageAdapter(Context c, ArrayList<String> listofImage, String image_url) {
        mContext = c;
        this.listofImage = listofImage;
        this.image_url = image_url;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return (listofImage.size()+1);
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ViewHolder holder;
        if (convertView == null) {

            //  convertView = new View(mContext);
            convertView = inflater.inflate(R.layout.grid_single, null);
            holder = new ViewHolder();
            holder.property_image = (ImageView)convertView.findViewById(R.id.grid_image);


        }  else
        {
            holder = (ViewHolder) convertView.getTag();

        }
        if(position!=0){

            System.out.println("xxxx yashal "+image_url+listofImage.get(position-1));
            ((RequestListingActivity)mContext).setImageInLayout(mContext, 90,90, image_url+listofImage.get(position-1), holder.property_image);

        }

        convertView.setTag(holder);


        return convertView;
    }
    // convert view class
    class ViewHolder
    {
        ImageView property_image;
    }
}
