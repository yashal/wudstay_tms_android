package com.wudstay.tms.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.MoreAmenitiesActivity;
import com.wudstay.tms.activity.ViewPropertyActivity;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class MoreAmenitiesAdapter extends RecyclerView.Adapter<MoreAmenitiesAdapter.ViewHolder> {

    private int height, width;

    private MoreAmenitiesActivity context;

    public MoreAmenitiesAdapter(MoreAmenitiesActivity context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.amenities_list_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView amenities_name;
        private ImageView amenities_icon;

        public ViewHolder(View itemView) {
            super(itemView);

            amenities_name = (TextView) itemView.findViewById(R.id.amenities_name);
            amenities_icon = (ImageView) itemView.findViewById(R.id.amenities_icon);

        }
    }
}
