package com.wudstay.tms.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.CreatePropertyActivity;
import com.wudstay.tms.activity.PropertyListDetailsActivity;
import com.wudstay.tms.activity.RequestListingActivity;
import com.wudstay.tms.pojo.OccupancyWiseNoOfRooms;
import com.wudstay.tms.pojo.OccupancyWiseRoomsPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class PgAdapter extends RecyclerView.Adapter<PgAdapter.ViewHolder> {

    private int height, width;
    private ArrayList<OccupancyWiseNoOfRooms>  pgType;


    private PropertyListDetailsActivity context;
    private CreatePropertyActivity ctx;
    private String from;

    public PgAdapter(Activity context, ArrayList<OccupancyWiseNoOfRooms>  pgType) {

            this.ctx =(CreatePropertyActivity)context;

        this.pgType = pgType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pg_list_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final OccupancyWiseNoOfRooms data  = pgType.get(position);
        holder.serial_no.setText(""+(position+1));
        if (data.getOccupancyName() != null && !data.getOccupancyName().isEmpty())
        {
            holder.occupancy_type.setText(data.getOccupancyName() );
        }

        holder.total_rooms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                    data.setNoOfRooms(Integer.parseInt(holder.total_rooms.getText().toString()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return pgType.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView occupancy_type, serial_no;
        private EditText total_rooms;

        public ViewHolder(View itemView) {
            super(itemView);
            occupancy_type = (TextView) itemView.findViewById(R.id.occupancy_type);
            serial_no = (TextView) itemView.findViewById(R.id.serial_no);
            total_rooms = (EditText) itemView.findViewById(R.id.total_rooms);

        }
    }
}
