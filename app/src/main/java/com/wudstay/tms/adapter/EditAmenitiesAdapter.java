package com.wudstay.tms.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.EditAmenitiesActivity;
import com.wudstay.tms.activity.MoreAmenitiesActivity;

/**
 * Created by quepplin1 on 2/9/2016.
 */
public class EditAmenitiesAdapter extends RecyclerView.Adapter<EditAmenitiesAdapter.ViewHolder> {

    private int height, width;

    private EditAmenitiesActivity context;

    public EditAmenitiesAdapter(EditAmenitiesActivity context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.amenities_edit_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView amenities_name;
        private ImageView amenities_icon;

        public ViewHolder(View itemView) {
            super(itemView);

            amenities_name = (TextView) itemView.findViewById(R.id.amenities_name);
            amenities_icon = (ImageView) itemView.findViewById(R.id.amenities_icon);

        }
    }
}
