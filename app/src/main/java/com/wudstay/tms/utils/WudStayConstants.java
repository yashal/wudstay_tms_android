package com.wudstay.tms.utils;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by arpit on 1/5/2017.
 */

public class WudStayConstants {

    public final static ArrayList<Activity> ACTIVITIES = new ArrayList<>();
    public final static String PREF_NAME = "com.wudstaytms.prefs";
    public final static String DEFAULT_VALUE = "";
    //    public final static String BASE_URL = "http://54.251.116.126:8080/wudstay/webservice/";
    public final static String BASE_URL = "http://54.251.116.126:8080/rentout/";
    public final static String NO_INTERNET_CONNECTED = "No internet connection available";
    public final static String EMAIL = "email";
    public final static String PASSWORD = "password";
    public final static String USER_NAME = "username";
    public final static String COOKIE_NAME = "COOKIENAME";
    public final static String TOKEN = "TOKEN";
    public final static String IMAGE_URL = "http://54.251.116.126:8080/rentout/imageresource/propertygallery/";
//    public final static String IMAGE_URL = http://www.wudstay.com/imageresource/propertygallery/";
}
