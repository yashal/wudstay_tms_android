package com.wudstay.tms.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wudstay.tms.R;
import com.wudstay.tms.activity.BaseActivity;
import com.wudstay.tms.activity.LoginActivity;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Administrator on 15-Feb-16.
 */
public class WudstayDialogs {
    private Dialog lDialog;
    private Activity ctx;
    public static final String PREFS_NAME = "MyPrefsFile1";

    public static ProgressDialog showLoading(Activity activity) {
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        if (!activity.isFinishing() && !mProgressDialog.isShowing())
            mProgressDialog.show();
        return mProgressDialog;
    }

    public void showProgressDialog(Activity activity) {
        lDialog = new Dialog(ctx, R.style.Theme_Dialog_Custom_My);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lDialog.setContentView(R.layout.dialog_loading);
        ImageView imgCard = (ImageView) lDialog.findViewById(R.id.imgCard);
        imgCard.startAnimation(
                AnimationUtils.loadAnimation(ctx, R.anim.wudstay_loading_animation) );
        if (!activity.isFinishing() && !lDialog.isShowing())
            lDialog.show();
    }
    public Dialog getlDialog() {
        return lDialog;
    }

    public WudstayDialogs(Activity ctx) {
        this.ctx = ctx;
    }

    public void displayCommonDialogWithHeaderSmall(String header, String msg) {
        TextView OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog_small);
        if (!DialogLogOut.isShowing()) {
            TextView msg_textView = (TextView) DialogLogOut.findViewById(R.id.text_exit);
            TextView dialog_header = (TextView) DialogLogOut.findViewById(R.id.dialog_header);
            dialog_header.setVisibility(View.VISIBLE);
            msg_textView.setText(msg);
            dialog_header.setText(header);
            OkButtonLogout = (TextView) DialogLogOut.findViewById(R.id.btn_yes_exit);
            ImageView dialog_header_cross = (ImageView) DialogLogOut.findViewById(R.id.dialog_header_cross);
            dialog_header_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogLogOut.dismiss();
                }
            });

            OkButtonLogout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    DialogLogOut.dismiss();
                }
            });
            DialogLogOut.show();
        }
    }

    public void displayDialogWithCancel(String msg) {
        LinearLayout OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog);
        TextView loogout_msg = (TextView) DialogLogOut.findViewById(R.id.text_exit);
        loogout_msg.setText(msg);
        OkButtonLogout = (LinearLayout) DialogLogOut.findViewById(R.id.btn_yes_exit_LL);
        LinearLayout CancelButtonLogout = (LinearLayout) DialogLogOut.findViewById(R.id.btn_no_exit_LL);

        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
                ((BaseActivity)ctx).Logout();
            }
        });
        CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });
        DialogLogOut.show();
    }

}
