package com.wudstay.tms.pojo;

/**
 * Created by yash on 4/18/2017.
 */

public class ListTenantPojo extends BasePojo {

    private ListTenantDataPojo responseObjects;

    public ListTenantDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(ListTenantDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
