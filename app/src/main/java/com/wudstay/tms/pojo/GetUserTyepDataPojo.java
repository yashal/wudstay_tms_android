package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 3/28/2017.
 */

public class GetUserTyepDataPojo {

    private ArrayList<UserTypeList> userTypeList;

    public ArrayList<UserTypeList> getUserTypeList() {
        return userTypeList;
    }

    public void setUserTypeList(ArrayList<UserTypeList> userTypeList) {
        this.userTypeList = userTypeList;
    }
}
