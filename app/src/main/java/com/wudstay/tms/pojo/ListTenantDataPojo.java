package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 4/18/2017.
 */

public class ListTenantDataPojo {

    private ArrayList<OccupancyList> occupancyList;
    private String agreement;
    private ArrayList<ParticularList> particularsList;
    private ArrayList<PropertiesListPojo> listOfProperties;

    public ArrayList<OccupancyList> getOccupancyList() {
        return occupancyList;
    }

    public void setOccupancyList(ArrayList<OccupancyList> occupancyList) {
        this.occupancyList = occupancyList;
    }

    public String getAgreement() {
        return agreement;
    }

    public void setAgreement(String agreement) {
        this.agreement = agreement;
    }

    public ArrayList<ParticularList> getParticularsList() {
        return particularsList;
    }

    public void setParticularsList(ArrayList<ParticularList> particularsList) {
        this.particularsList = particularsList;
    }

    public ArrayList<PropertiesListPojo> getListOfProperties() {
        return listOfProperties;
    }

    public void setListOfProperties(ArrayList<PropertiesListPojo> listOfProperties) {
        this.listOfProperties = listOfProperties;
    }
}
