package com.wudstay.tms.pojo;

/**
 * Created by yash on 5/2/2017.
 */

public class ComplainPojo extends BasePojo {

    private ComplainDataPojo responseObjects;

    public ComplainDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(ComplainDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
