package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 5/2/2017.
 */

public class PropertyDto {

    private int id;
    private boolean active;
    private int propertyOwnerId;
    private int propertyTypeId;
    private String propertyTypeName;
    private String propertyName;
    private String propertyAddress;
    private int cityId;
    private String cityName;
    private String locationName;
    private String pincode;
    private String category;
    private String flatType;
    private String propertyImageName;
    private int listingStatus;
    private int noOfImagesOfThisProperty;
    private int propertyId;
    private ArrayList<String> images;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getPropertyOwnerId() {
        return propertyOwnerId;
    }

    public void setPropertyOwnerId(int propertyOwnerId) {
        this.propertyOwnerId = propertyOwnerId;
    }

    public int getPropertyTypeId() {
        return propertyTypeId;
    }

    public void setPropertyTypeId(int propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }

    public String getPropertyTypeName() {
        return propertyTypeName;
    }

    public void setPropertyTypeName(String propertyTypeName) {
        this.propertyTypeName = propertyTypeName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyAddress() {
        return propertyAddress;
    }

    public void setPropertyAddress(String propertyAddress) {
        this.propertyAddress = propertyAddress;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFlatType() {
        return flatType;
    }

    public void setFlatType(String flatType) {
        this.flatType = flatType;
    }

    public String getPropertyImageName() {
        return propertyImageName;
    }

    public void setPropertyImageName(String propertyImageName) {
        this.propertyImageName = propertyImageName;
    }

    public int getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(int listingStatus) {
        this.listingStatus = listingStatus;
    }

    public int getNoOfImagesOfThisProperty() {
        return noOfImagesOfThisProperty;
    }

    public void setNoOfImagesOfThisProperty(int noOfImagesOfThisProperty) {
        this.noOfImagesOfThisProperty = noOfImagesOfThisProperty;
    }

    public int getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
