package com.wudstay.tms.pojo;

/**
 * Created by yash on 5/2/2017.
 */

public class OwnerLsitProperties {

    private int id;
    private String description;
    private String complaintType;
    private String active;
    private String createdByUser;
    private String createdDate;
    private String lastModifiedDate;
    private TeanantOrUser tenantOrUser;
    private PropertyDto propertyDto;
    private String latestStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComplaintType() {
        return complaintType;
    }

    public void setComplaintType(String complaintType) {
        this.complaintType = complaintType;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public TeanantOrUser getTenantOrUser() {
        return tenantOrUser;
    }

    public void setTenantOrUser(TeanantOrUser tenantOrUser) {
        this.tenantOrUser = tenantOrUser;
    }

    public PropertyDto getPropertyDto() {
        return propertyDto;
    }

    public void setPropertyDto(PropertyDto propertyDto) {
        this.propertyDto = propertyDto;
    }

    public String getLatestStatus() {
        return latestStatus;
    }

    public void setLatestStatus(String latestStatus) {
        this.latestStatus = latestStatus;
    }
}
