package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 4/10/2017.
 */

public class PropertyFormPojo {

    private ArrayList<OccupancyWiseNoOfRooms> occupancyWiseNoOfRooms;

    public ArrayList<OccupancyWiseNoOfRooms> getOccupancyWiseNoOfRooms() {
        return occupancyWiseNoOfRooms;
    }

    public void setOccupancyWiseNoOfRooms(ArrayList<OccupancyWiseNoOfRooms> occupancyWiseNoOfRooms) {
        this.occupancyWiseNoOfRooms = occupancyWiseNoOfRooms;
    }
}
