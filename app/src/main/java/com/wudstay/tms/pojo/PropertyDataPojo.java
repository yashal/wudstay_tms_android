package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 3/29/2017.
 */

public class PropertyDataPojo {

    private ArrayList<PropertiesListPojo> listOfProperties;

    public ArrayList<PropertiesListPojo> getListOfProperties() {
        return listOfProperties;
    }

    public void setListOfProperties(ArrayList<PropertiesListPojo> listOfProperties) {
        this.listOfProperties = listOfProperties;
    }
}
