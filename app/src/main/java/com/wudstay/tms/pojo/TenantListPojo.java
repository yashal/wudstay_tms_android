package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 4/18/2017.
 */

public class TenantListPojo extends OtherBasePojo {

    private ArrayList<TenantDtoListPojo> tenantDtoList;

    public ArrayList<TenantDtoListPojo> getTenantDtoList() {
        return tenantDtoList;
    }

    public void setTenantDtoList(ArrayList<TenantDtoListPojo> tenantDtoList) {
        this.tenantDtoList = tenantDtoList;
    }
}
