package com.wudstay.tms.pojo;

/**
 * Created by yash on 4/19/2017.
 */

public class CreateTPojo extends OtherBasePojo {

    private int tenantId;

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }
}
