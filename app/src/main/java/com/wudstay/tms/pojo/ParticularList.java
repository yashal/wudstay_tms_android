package com.wudstay.tms.pojo;

/**
 * Created by yash on 4/18/2017.
 */

public class ParticularList {

    private Integer id;
    private String particularName;
    private String type;
    private String defaultRemark;
    private boolean active;
    private String amount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParticularName() {
        return particularName;
    }

    public void setParticularName(String particularName) {
        this.particularName = particularName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDefaultRemark() {
        return defaultRemark;
    }

    public void setDefaultRemark(String defaultRemark) {
        this.defaultRemark = defaultRemark;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
