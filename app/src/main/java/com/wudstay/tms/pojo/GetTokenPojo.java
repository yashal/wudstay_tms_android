package com.wudstay.tms.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yash on 3/9/2017.
 */

public class GetTokenPojo extends BasePojo{

    private GetTokenDataPojo responseObjects;

    public GetTokenDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(GetTokenDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
