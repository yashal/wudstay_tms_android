package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 4/18/2017.
 */

public class TenantPropertyListDataPojo {

    private int propertyId;
    private ArrayList<PropertiesListPojo> listOfProperties;

    public int getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    public ArrayList<PropertiesListPojo> getListOfProperties() {
        return listOfProperties;
    }

    public void setListOfProperties(ArrayList<PropertiesListPojo> listOfProperties) {
        this.listOfProperties = listOfProperties;
    }
}
