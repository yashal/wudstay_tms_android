package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/23/2017.
 */

public class TenantDataPojo {

    private int id;
    private String tenantName;
    private String tenantEmail;
    private String tenantMobile;
    private boolean active;
    private int createdByUserId;
    private String occupancy;
    private String propertyId;
    private String address;
    private String gender;
//    private String tenantEmergencyDetailDtos;
//    private String tenantPgDetailsDtos;
//    private String tenantDocumentsDtos;
//    private String appUserId;
//    private String createdByDate;
//    private String tenantAgreementDtos;
//    private String agreementAppend;
//    private String propertyTenantDtos;
//    private String tenantNoticeDtos;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantEmail() {
        return tenantEmail;
    }

    public void setTenantEmail(String tenantEmail) {
        this.tenantEmail = tenantEmail;
    }

    public String getTenantMobile() {
        return tenantMobile;
    }

    public void setTenantMobile(String tenantMobile) {
        this.tenantMobile = tenantMobile;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(int createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
