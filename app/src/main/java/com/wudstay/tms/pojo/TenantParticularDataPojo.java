package com.wudstay.tms.pojo;

/**
 * Created by Yash on 4/24/2017.
 */

public class TenantParticularDataPojo {

    private int tenantId;
    private String tenantName;
    private String tenantEmail;
    private String tenantMobile;

    private int roomParticularId;
    private String roomParticularName;
    private int roomAmount;

    private int foodParticularId;
    private String foodParticularName;
    private int foodAmount;

    private int wifiParticularId;
    private String wifiParticularName;
    private int wifiAmount;
    private boolean status = false;

    private int totalAmount;
    private int balance;
    private int paymentId;
    private int prevAmount;
    private int paidAmount;
    private int recptAmount;

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantEmail() {
        return tenantEmail;
    }

    public void setTenantEmail(String tenantEmail) {
        this.tenantEmail = tenantEmail;
    }

    public String getTenantMobile() {
        return tenantMobile;
    }

    public void setTenantMobile(String tenantMobile) {
        this.tenantMobile = tenantMobile;
    }

    public int getRoomParticularId() {
        return roomParticularId;
    }

    public void setRoomParticularId(int roomParticularId) {
        this.roomParticularId = roomParticularId;
    }

    public String getRoomParticularName() {
        return roomParticularName;
    }

    public void setRoomParticularName(String roomParticularName) {
        this.roomParticularName = roomParticularName;
    }

    public int getRoomAmount() {
        return roomAmount;
    }

    public void setRoomAmount(int roomAmount) {
        this.roomAmount = roomAmount;
    }

    public int getFoodParticularId() {
        return foodParticularId;
    }

    public void setFoodParticularId(int foodParticularId) {
        this.foodParticularId = foodParticularId;
    }

    public String getFoodParticularName() {
        return foodParticularName;
    }

    public void setFoodParticularName(String foodParticularName) {
        this.foodParticularName = foodParticularName;
    }

    public int getFoodAmount() {
        return foodAmount;
    }

    public void setFoodAmount(int foodAmount) {
        this.foodAmount = foodAmount;
    }

    public int getWifiParticularId() {
        return wifiParticularId;
    }

    public void setWifiParticularId(int wifiParticularId) {
        this.wifiParticularId = wifiParticularId;
    }

    public String getWifiParticularName() {
        return wifiParticularName;
    }

    public void setWifiParticularName(String wifiParticularName) {
        this.wifiParticularName = wifiParticularName;
    }

    public int getWifiAmount() {
        return wifiAmount;
    }

    public void setWifiAmount(int wifiAmount) {
        this.wifiAmount = wifiAmount;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getPrevAmount() {
        return prevAmount;
    }

    public void setPrevAmount(int prevAmount) {
        this.prevAmount = prevAmount;
    }

    public int getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(int paidAmount) {
        this.paidAmount = paidAmount;
    }

    public int getRecptAmount() {
        return recptAmount;
    }

    public void setRecptAmount(int recptAmount) {
        this.recptAmount = recptAmount;
    }
}
