package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 3/23/2017.
 */

public class LoginUserDataPojo {

    private int appUserId;
    private String displayName;
    private String managerId;
    private String state;
    private ArrayList<LoginUserRolPojo> userRoles;
    private String registrationKey;
    private String activationKey;
    private TenantDataPojo tenantDto;
    private String encodedActivationKeyForUrl;
    private boolean tenantActive;
    private String tenantOccupancy;
    private String tenantPropertyId;
    private String tenantAddress;
    private int tenantId;
    private String tenantName;
    private String tenantEmail;
    private String tenantMobile;
//    private String tenantEmergencyDetailDtos;


    public int getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(int appUserId) {
        this.appUserId = appUserId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<LoginUserRolPojo> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(ArrayList<LoginUserRolPojo> userRoles) {
        this.userRoles = userRoles;
    }

    public String getRegistrationKey() {
        return registrationKey;
    }

    public void setRegistrationKey(String registrationKey) {
        this.registrationKey = registrationKey;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public TenantDataPojo getTenantDto() {
        return tenantDto;
    }

    public void setTenantDto(TenantDataPojo tenantDto) {
        this.tenantDto = tenantDto;
    }

    public String getEncodedActivationKeyForUrl() {
        return encodedActivationKeyForUrl;
    }

    public void setEncodedActivationKeyForUrl(String encodedActivationKeyForUrl) {
        this.encodedActivationKeyForUrl = encodedActivationKeyForUrl;
    }

    public boolean isTenantActive() {
        return tenantActive;
    }

    public void setTenantActive(boolean tenantActive) {
        this.tenantActive = tenantActive;
    }

    public String getTenantOccupancy() {
        return tenantOccupancy;
    }

    public void setTenantOccupancy(String tenantOccupancy) {
        this.tenantOccupancy = tenantOccupancy;
    }

    public String getTenantPropertyId() {
        return tenantPropertyId;
    }

    public void setTenantPropertyId(String tenantPropertyId) {
        this.tenantPropertyId = tenantPropertyId;
    }

    public String getTenantAddress() {
        return tenantAddress;
    }

    public void setTenantAddress(String tenantAddress) {
        this.tenantAddress = tenantAddress;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantEmail() {
        return tenantEmail;
    }

    public void setTenantEmail(String tenantEmail) {
        this.tenantEmail = tenantEmail;
    }

    public String getTenantMobile() {
        return tenantMobile;
    }

    public void setTenantMobile(String tenantMobile) {
        this.tenantMobile = tenantMobile;
    }
}
