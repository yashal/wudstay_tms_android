package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 4/19/2017.
 */

public class ViewDocsDataPojo {

    private ArrayList<ViewDocTenantPojo> tenantDocs;
    private boolean status;

    public ArrayList<ViewDocTenantPojo> getTenantDocs() {
        return tenantDocs;
    }

    public void setTenantDocs(ArrayList<ViewDocTenantPojo> tenantDocs) {
        this.tenantDocs = tenantDocs;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
