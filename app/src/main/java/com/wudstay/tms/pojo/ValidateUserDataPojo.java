package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/24/2017.
 */

public class ValidateUserDataPojo {

    private boolean DEFAULT_RESPONSE_OBJECT;
    private OtpInfoPojo OTP_INFO;

    public boolean isDEFAULT_RESPONSE_OBJECT() {
        return DEFAULT_RESPONSE_OBJECT;
    }

    public void setDEFAULT_RESPONSE_OBJECT(boolean DEFAULT_RESPONSE_OBJECT) {
        this.DEFAULT_RESPONSE_OBJECT = DEFAULT_RESPONSE_OBJECT;
    }

    public OtpInfoPojo getOTP_INFO() {
        return OTP_INFO;
    }

    public void setOTP_INFO(OtpInfoPojo OTP_INFO) {
        this.OTP_INFO = OTP_INFO;
    }
}
