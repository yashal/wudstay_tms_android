package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/23/2017.
 */

public class DashboardPojo extends BasePojo {

    private DashboardDataPojo responseObjects;

    public DashboardDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(DashboardDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
