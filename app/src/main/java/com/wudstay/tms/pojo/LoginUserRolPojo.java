package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/23/2017.
 */

public class LoginUserRolPojo {

    private int key;
    private String value;
    private String otherValue;

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOtherValue() {
        return otherValue;
    }

    public void setOtherValue(String otherValue) {
        this.otherValue = otherValue;
    }
}
