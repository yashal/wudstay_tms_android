package com.wudstay.tms.pojo;

/**
 * Created by yash on 4/18/2017.
 */

public class TenantDtoListPojo {

    private int id;
    private String tenantName;
    private String tenantEmail;
    private String tenantMobile;
    private boolean active;
    private int propertyId;
    private String address;
    private String gender;
    private long createdByDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantEmail() {
        return tenantEmail;
    }

    public void setTenantEmail(String tenantEmail) {
        this.tenantEmail = tenantEmail;
    }

    public String getTenantMobile() {
        return tenantMobile;
    }

    public void setTenantMobile(String tenantMobile) {
        this.tenantMobile = tenantMobile;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getCreatedByDate() {
        return createdByDate;
    }

    public void setCreatedByDate(long createdByDate) {
        this.createdByDate = createdByDate;
    }
}
