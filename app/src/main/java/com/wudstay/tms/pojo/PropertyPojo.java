package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/29/2017.
 */

public class PropertyPojo extends BasePojo{

    private PropertyDataPojo responseObjects;

    public PropertyDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(PropertyDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
