package com.wudstay.tms.pojo;

import java.io.Serializable;

/**
 * Created by yash on 3/29/2017.
 */

public class OccupancyWiseRoomsPojo implements Serializable{

    private int occupancyTypeId;
    private boolean active;
    private String occupancyName;
    private Integer noOfBeds;
    private Integer noOfRooms;
    private String category;

    public int getOccupancyTypeId() {
        return occupancyTypeId;
    }

    public void setOccupancyTypeId(int occupancyTypeId) {
        this.occupancyTypeId = occupancyTypeId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getOccupancyName() {
        return occupancyName;
    }

    public void setOccupancyName(String occupancyName) {
        this.occupancyName = occupancyName;
    }

    public Integer getNoOfBeds() {
        return noOfBeds;
    }

    public void setNoOfBeds(Integer noOfBeds) {
        this.noOfBeds = noOfBeds;
    }

    public Integer getNoOfRooms() {
        return noOfRooms;
    }

    public void setNoOfRooms(Integer noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
