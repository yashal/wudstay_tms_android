package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/28/2017.
 */

public class GetUserTyepPojo extends BasePojo{

    private GetUserTyepDataPojo responseObjects;

    public GetUserTyepDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(GetUserTyepDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
