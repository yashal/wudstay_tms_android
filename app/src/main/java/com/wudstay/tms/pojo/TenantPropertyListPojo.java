package com.wudstay.tms.pojo;

/**
 * Created by yash on 4/18/2017.
 */

public class TenantPropertyListPojo extends BasePojo {

    private TenantPropertyListDataPojo responseObjects;

    public TenantPropertyListDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(TenantPropertyListDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
