package com.wudstay.tms.pojo;

/**
 * Created by yash on 4/10/2017.
 */

public class GetPropertyPojo extends BasePojo {

    private GetPropertyResponsePojo responseObjects;

    public GetPropertyResponsePojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(GetPropertyResponsePojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
