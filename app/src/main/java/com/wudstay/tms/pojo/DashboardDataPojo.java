package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/23/2017.
 */

public class DashboardDataPojo {

    private int pMonthBal;
    private String pMonthUrl;
    private String nMonthUrl;
    private String cMonth;
    private String pMonth;
    private int countAcTransactions;
    private String cMonthUrl;
    private String nMonth;
    private Integer countBookings;
    private boolean TENANT_WITHOUT_PROPERTY;
    private Integer cMonthAmt;
    private Integer pMonthAmt;
    private String loggedInUserDisplayName;
    private LoginUserDataPojo loggedInUserDto;
    private Integer countProperties;
    private Integer countComplaints;
    private Integer countNotifications;
    private Integer nMonthAmt;
    private int cMonthBal;
    private int nMonthBal;

    public int getpMonthBal() {
        return pMonthBal;
    }

    public void setpMonthBal(int pMonthBal) {
        this.pMonthBal = pMonthBal;
    }

    public String getpMonthUrl() {
        return pMonthUrl;
    }

    public void setpMonthUrl(String pMonthUrl) {
        this.pMonthUrl = pMonthUrl;
    }

    public String getnMonthUrl() {
        return nMonthUrl;
    }

    public void setnMonthUrl(String nMonthUrl) {
        this.nMonthUrl = nMonthUrl;
    }

    public String getcMonth() {
        return cMonth;
    }

    public void setcMonth(String cMonth) {
        this.cMonth = cMonth;
    }

    public String getpMonth() {
        return pMonth;
    }

    public void setpMonth(String pMonth) {
        this.pMonth = pMonth;
    }

    public int getCountAcTransactions() {
        return countAcTransactions;
    }

    public void setCountAcTransactions(int countAcTransactions) {
        this.countAcTransactions = countAcTransactions;
    }

    public String getcMonthUrl() {
        return cMonthUrl;
    }

    public void setcMonthUrl(String cMonthUrl) {
        this.cMonthUrl = cMonthUrl;
    }

    public String getnMonth() {
        return nMonth;
    }

    public void setnMonth(String nMonth) {
        this.nMonth = nMonth;
    }

    public Integer getCountBookings() {
        return countBookings;
    }

    public void setCountBookings(Integer countBookings) {
        this.countBookings = countBookings;
    }

    public boolean isTENANT_WITHOUT_PROPERTY() {
        return TENANT_WITHOUT_PROPERTY;
    }

    public void setTENANT_WITHOUT_PROPERTY(boolean TENANT_WITHOUT_PROPERTY) {
        this.TENANT_WITHOUT_PROPERTY = TENANT_WITHOUT_PROPERTY;
    }

    public Integer getcMonthAmt() {
        return cMonthAmt;
    }

    public void setcMonthAmt(Integer cMonthAmt) {
        this.cMonthAmt = cMonthAmt;
    }

    public Integer getpMonthAmt() {
        return pMonthAmt;
    }

    public void setpMonthAmt(Integer pMonthAmt) {
        this.pMonthAmt = pMonthAmt;
    }

    public String getLoggedInUserDisplayName() {
        return loggedInUserDisplayName;
    }

    public void setLoggedInUserDisplayName(String loggedInUserDisplayName) {
        this.loggedInUserDisplayName = loggedInUserDisplayName;
    }

    public LoginUserDataPojo getLoggedInUserDto() {
        return loggedInUserDto;
    }

    public void setLoggedInUserDto(LoginUserDataPojo loggedInUserDto) {
        this.loggedInUserDto = loggedInUserDto;
    }

    public Integer getCountProperties() {
        return countProperties;
    }

    public void setCountProperties(Integer countProperties) {
        this.countProperties = countProperties;
    }

    public Integer getCountComplaints() {
        return countComplaints;
    }

    public void setCountComplaints(Integer countComplaints) {
        this.countComplaints = countComplaints;
    }

    public Integer getCountNotifications() {
        return countNotifications;
    }

    public void setCountNotifications(Integer countNotifications) {
        this.countNotifications = countNotifications;
    }

    public Integer getnMonthAmt() {
        return nMonthAmt;
    }

    public void setnMonthAmt(Integer nMonthAmt) {
        this.nMonthAmt = nMonthAmt;
    }

    public int getcMonthBal() {
        return cMonthBal;
    }

    public void setcMonthBal(int cMonthBal) {
        this.cMonthBal = cMonthBal;
    }

    public int getnMonthBal() {
        return nMonthBal;
    }

    public void setnMonthBal(int nMonthBal) {
        this.nMonthBal = nMonthBal;
    }
}
