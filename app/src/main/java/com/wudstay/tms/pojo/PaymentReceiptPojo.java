package com.wudstay.tms.pojo;

/**
 * Created by yash on 5/1/2017.
 */

public class PaymentReceiptPojo extends BasePojo {

    private PaymentReceiptDataPojo responseObjects;

    public PaymentReceiptDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(PaymentReceiptDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
