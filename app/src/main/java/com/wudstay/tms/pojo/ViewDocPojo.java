package com.wudstay.tms.pojo;

/**
 * Created by yash on 4/19/2017.
 */

public class ViewDocPojo extends BasePojo {

    private ViewDocsDataPojo responseObjects;

    public ViewDocsDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(ViewDocsDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
