package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/22/2017.
 */

public class LoginPojo extends BasePojo {

    private LoginDataPojo responseObjects;

    public LoginDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(LoginDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
