package com.wudstay.tms.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yash on 3/14/2017.
 */

public class GetTokenDataPojo {

    @Expose
    @SerializedName("XCSRFTOKEN")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
