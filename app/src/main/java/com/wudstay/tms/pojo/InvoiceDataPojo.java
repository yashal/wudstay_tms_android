package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by Yash on 4/24/2017.
 */

public class InvoiceDataPojo {

    private ArrayList<PropertiesListPojo> listOfProperties;

    public ArrayList<PropertiesListPojo> getListOfProperties() {
        return listOfProperties;
    }

    public void setListOfProperties(ArrayList<PropertiesListPojo> listOfProperties) {
        this.listOfProperties = listOfProperties;
    }
}
