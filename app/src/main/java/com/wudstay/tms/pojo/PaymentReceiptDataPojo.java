package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 5/1/2017.
 */

public class PaymentReceiptDataPojo {

    private ArrayList<TenantDataPojo> tenantList;
    private ArrayList<PropertiesListPojo> listOfProperties;

    public ArrayList<TenantDataPojo> getTenantList() {
        return tenantList;
    }

    public void setTenantList(ArrayList<TenantDataPojo> tenantList) {
        this.tenantList = tenantList;
    }

    public ArrayList<PropertiesListPojo> getListOfProperties() {
        return listOfProperties;
    }

    public void setListOfProperties(ArrayList<PropertiesListPojo> listOfProperties) {
        this.listOfProperties = listOfProperties;
    }
}
