package com.wudstay.tms.pojo;

/**
 * Created by Yash on 4/24/2017.
 */

public class InvoicePojo extends BasePojo {

    private InvoiceDataPojo responseObjects;

    public InvoiceDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(InvoiceDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
