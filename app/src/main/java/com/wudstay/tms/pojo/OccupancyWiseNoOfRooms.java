package com.wudstay.tms.pojo;

/**
 * Created by yash on 4/10/2017.
 */

public class OccupancyWiseNoOfRooms {

    private int occupancyTypeId;
    private Integer noOfBeds;
    private Integer noOfRooms;
    private boolean active;
    private String occupancyName;
    private String category;

    public int getOccupancyTypeId() {
        return occupancyTypeId;
    }

    public void setOccupancyTypeId(int occupancyTypeId) {
        this.occupancyTypeId = occupancyTypeId;
    }

    public Integer getNoOfBeds() {
        return noOfBeds;
    }

    public void setNoOfBeds(Integer noOfBeds) {
        this.noOfBeds = noOfBeds;
    }

    public Integer getNoOfRooms() {
        return noOfRooms;
    }

    public void setNoOfRooms(Integer noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getOccupancyName() {
        return occupancyName;
    }

    public void setOccupancyName(String occupancyName) {
        this.occupancyName = occupancyName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
