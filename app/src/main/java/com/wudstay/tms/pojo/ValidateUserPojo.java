package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/24/2017.
 */

public class ValidateUserPojo extends BasePojo {

    private ValidateUserDataPojo responseObjects;

    public ValidateUserDataPojo getResponseObjects() {
        return responseObjects;
    }

    public void setResponseObjects(ValidateUserDataPojo responseObjects) {
        this.responseObjects = responseObjects;
    }
}
