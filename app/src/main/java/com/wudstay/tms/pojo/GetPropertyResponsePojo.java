package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 4/10/2017.
 */

public class GetPropertyResponsePojo {

    private ArrayList<CityListPojo> ListOfCities;
    private PropertyFormPojo propertyForm;

    public ArrayList<CityListPojo> getListOfCities() {
        return ListOfCities;
    }

    public void setListOfCities(ArrayList<CityListPojo> listOfCities) {
        ListOfCities = listOfCities;
    }

    public PropertyFormPojo getPropertyForm() {
        return propertyForm;
    }

    public void setPropertyForm(PropertyFormPojo propertyForm) {
        this.propertyForm = propertyForm;
    }
}
