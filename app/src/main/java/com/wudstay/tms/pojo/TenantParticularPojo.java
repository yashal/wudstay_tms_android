package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by Yash on 4/24/2017.
 */

public class TenantParticularPojo extends OtherBasePojo {

    private ArrayList<TenantParticularDataPojo> tenantParticularsDtos;

    public ArrayList<TenantParticularDataPojo> getTenantParticularsDtos() {
        return tenantParticularsDtos;
    }

    public void setTenantParticularsDtos(ArrayList<TenantParticularDataPojo> tenantParticularsDtos) {
        this.tenantParticularsDtos = tenantParticularsDtos;
    }
}
