package com.wudstay.tms.pojo;

/**
 * Created by yash on 3/22/2017.
 */

public class LoginDataPojo {

    private boolean auth;

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }
}
