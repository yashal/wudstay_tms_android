package com.wudstay.tms.pojo;

/**
 * Created by arpit on 1/5/2017.
 */

public class BasePojo {

    private String message;
    private String title;
    private int responseCode;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

}
