package com.wudstay.tms.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 5/2/2017.
 */

public class ComplainDataPojo {

    private ArrayList<OwnerLsitProperties> listOfOwnProperties;

    public ArrayList<OwnerLsitProperties> getListOfOwnProperties() {
        return listOfOwnProperties;
    }

    public void setListOfOwnProperties(ArrayList<OwnerLsitProperties> listOfOwnProperties) {
        this.listOfOwnProperties = listOfOwnProperties;
    }
}
